import { NgModule } from '@angular/core';
import { canActivate, customClaims, redirectLoggedInTo } from '@angular/fire/auth-guard';
import { RouterModule, Routes } from '@angular/router';
import { pipe } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthGuard } from '@cores/auth-guard.service';

import { BannedComponent } from './views/banned.component';
import { DeletedComponent } from './views/deleted.component';
import { HomeComponent } from './views/home.component';
import { LoginComponent } from './views/login.component';
import { LogoutComponent } from './views/logout.component';
import { PageNotFoundComponent } from './views/not-found.component';
import { UpdateComponent } from './views/reg/update.component';
import { VerifyComponent } from './views/reg/verify.component';

const authorized = redirectLoggedInTo(['products']);
const banned = pipe(customClaims, map(claims => !!claims.banned ? true : ['products']));
const deleted = pipe(customClaims, map(claims => !!claims.deleted ? true : ['products']));
const validUser = pipe(customClaims, map(claims => !!claims.deleted ? ['deleted'] : (!!claims.banned ? ['banHammer'] : true)));

export const AppRoutes: Routes = [
  { path: 'banHammer', component: BannedComponent, ...canActivate(banned) },
  { path: 'deleted', component: DeletedComponent, ...canActivate(deleted) },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent, ...canActivate(authorized) },
  { path: 'logout', component: LogoutComponent },
  { path: 'products', loadChildren: () => import('app/products/products.module').then(m => m.ProductsModule) },
  { path: 'update', component: UpdateComponent, canActivate: [AuthGuard] },
  { path: 'verify', component: VerifyComponent, ...canActivate(validUser) },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(AppRoutes, {
    paramsInheritanceStrategy: 'always',
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRouting { }
