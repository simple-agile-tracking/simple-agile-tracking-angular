import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { AuthService } from '@cores/auth.service';
import { Message, MessageService } from '@cores/message.service';

@Component({
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
  email: string;
  regForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<RegisterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string,
    private readonly authService: AuthService,
    private readonly msgService: MessageService,
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.email = this.data;
    this.regForm = this.formBuilder.group({
      email: [this.email, [Validators.required, Validators.email]],
      displayName: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get emailFld() { return this.regForm.get('email'); }
  get displayName() { return this.regForm.get('displayName'); }
  get password() { return this.regForm.get('password'); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.authService.createRegular(this.regForm.value).then(
      (rtn: Message) => this.msgService.publish({ ...rtn, title: 'Error Saving New Registration' })
    );
  }
}
