import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';

@Component({
  selector: 'sat-header',
  templateUrl: 'header.component.html'
})
export class HeaderComponent {
  isLoggedIn = false;

  constructor(private readonly firebaseAuth: AngularFireAuth, private readonly router: Router) {
    this.firebaseAuth.authState.subscribe((user: User) => this.isLoggedIn = (!!user && user.emailVerified));
  }

  logout(): void {
    this.router.navigate(['/logout']);
  }
}
