import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';

@Component({
  templateUrl: 'home.component.html'
})
export class HomeComponent {
  isLoggedIn = false;
  thisUser = '';
  title = 'Welcome';
  msg = '';

  constructor(private readonly firebaseAuth: AngularFireAuth) {
    this.firebaseAuth.authState.subscribe(
      (user: User) => {
        if (user) {
          this.isLoggedIn = user.emailVerified;
          this.thisUser = user.displayName;
          this.msg = 'Welcome back, ' + this.thisUser + '!';
        } else {
          this.isLoggedIn = false;
          this.thisUser = '';
          this.msg = 'Welcome, strange user, please log in.';
        }
      }
    );
  }
}
