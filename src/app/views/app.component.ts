import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';

import { MessageService, FlexMessage } from '@cores/message.service';

import { DialogComponent } from '@shares/dialog.component';

@Component({
  selector: 'sat-app',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
  private subscription: Subscription;

  constructor(
    public dialog: MatDialog,
    public snack: MatSnackBar,
    private readonly msgService: MessageService
  ) { }

  ngOnInit() {
    this.subscription = this.msgService.push.subscribe(
      (msg: FlexMessage) => {
        if (msg) {
          if (msg.success) {
            this.snack.open(msg.message, 'Got it!', { duration: 5000 });
          } else {
            this.openDialog(msg.title, msg.message);
          }
        }
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private openDialog(ttl: string, msg: string): void {
    this.dialog.open(
      DialogComponent,
      {
        width: '400px',
        data: {
          title: ttl,
          content: msg
        }
      }
    );
  }
}
