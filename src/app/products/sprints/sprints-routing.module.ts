import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SprintComponent } from './sprint.component';
import { SprintListComponent } from './sprint-list.component';

export const SprintRoutes: Routes = [
  { path: ':id', component: SprintComponent },
  { path: '', component: SprintListComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(SprintRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class SprintRouting { }
