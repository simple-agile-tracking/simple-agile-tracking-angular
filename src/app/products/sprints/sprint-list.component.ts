import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { Message } from '@cores/message.service';

import { Sprint, SprintService } from './services/sprint.service';

import { SprintEditComponent } from './sprint-edit.component';

@Component({
  templateUrl: './sprint-list.component.html'
})
export class SprintListComponent implements OnInit {
  @ViewChild(MatTable, { static: true }) table: MatTable<Sprint>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  productId: string;
  hasSprints = false;
  displayedColumns = ['SprintNumber', 'Goal', 'StartDate', 'EndDate', 'ReleaseNumber', 'Actions'];
  showSpinner = false;
  private sprintData: MatTableDataSource<Sprint>;

  constructor(
    public dialog: MatDialog,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly sprintService: SprintService
  ) { }

  ngOnInit() {
    this.productId = this.route.snapshot.paramMap.get('prodid');
    this.getData();
  }

  applyFilter(filterValue: string): void {
    this.sprintData.filter = filterValue.trim().toLowerCase();
  }

  onAdd(): void {
    const dialogRef: MatDialogRef<SprintEditComponent> = this.dialog.open(
      SprintEditComponent,
      {
        width: '500px',
        data: {
          isNew: true,
          productId: this.productId
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onDelete(sprint: Sprint): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Sprint'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.sprintService.delete(this.productId, sprint) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.router.navigate(['/products', 'p', this.productId, 'sprints']);
      }
    });
  }

  onEdit(sprint: Sprint): void {
    const dialogRef: MatDialogRef<SprintEditComponent> = this.dialog.open(
      SprintEditComponent,
      {
        width: '500px',
        data: {
          isNew: false,
          productId: this.productId,
          sprint
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  private getData(): void {
    this.showSpinner = true;
    this.sprintService.getAll(this.productId).subscribe(
      (sprints: Sprint[]) => {
        this.hasSprints = !!sprints.length;
        this.sprintData = new MatTableDataSource(sprints);
        this.sprintData.sort = this.sort;
        this.table.dataSource = this.sprintData;
        this.showSpinner = false;
      },
      () => {
        this.hasSprints = false;
        this.showSpinner = false;
      }
    );
  }
}
