import { Component, Input } from '@angular/core';

import { Sprint } from './services/sprint.service';

@Component({
  selector: 'sat-release-details',
  templateUrl: './release-details.component.html'
})
export class ReleaseDetailsComponent {
  @Input() sprint: Sprint;
}
