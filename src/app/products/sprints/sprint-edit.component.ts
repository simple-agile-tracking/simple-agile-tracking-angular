import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { IdService } from '@cores/id.service';
import { Message } from '@cores/message.service';

import { Sprint, SprintService } from './services/sprint.service';

import { Release, ReleaseService } from '../releases/services/release.service';

@Component({
  templateUrl: './sprint-edit.component.html'
})
export class SprintEditComponent implements OnInit {
  sprtForm: FormGroup;
  releases: Release[];
  isNew: boolean;
  title: string;
  private productId: string;
  private sprint: Sprint;

  constructor(
    public dialogRef: MatDialogRef<SprintEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { isNew: boolean, productId: string, sprint: Sprint },
    private readonly sprintService: SprintService,
    private readonly relService: ReleaseService,
    private readonly formBuilder: FormBuilder,
    private readonly idService: IdService
  ) { }

  ngOnInit() {
    this.initForm(null);
    this.productId = this.data.productId;
    this.isNew = this.data.isNew;
    this.title = (this.isNew ? 'Add' : 'Edit') + ' Sprint';
    this.relService.getAll(this.productId).subscribe(
      (releases: Release[]) => {
        this.releases = releases;

        if (this.data.sprint) {
          this.sprint = this.data.sprint;
        }

        this.initForm(this.sprint);
      }
    );
  }

  get SprintNumber() { return this.sprtForm.get('SprintNumber'); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const newSprint = this.sprtForm.value;
    let saver: Observable<Message>;

    if (this.isNew) {
      newSprint.Id = this.idService.newID();
      saver = this.sprintService.insert(this.productId, newSprint);
    } else {
      saver = this.sprintService.update(this.productId, this.sprint, newSprint);
    }

    saver.subscribe((rtn: Message) => {
      if (rtn.success) {
        this.dialogRef.close('ok');
      }
    });
  }

  private initForm(sprint: Sprint): void {
    const sprtNmb: number = sprint ? sprint.SprintNumber : null;
    const sprtGoal = sprint ? sprint.Goal : '';
    const sprtStart: Date = sprint ? sprint.StartDate : null;
    const sprtEnd: Date = sprint ? sprint.EndDate : null;
    const sprtRel = sprint && sprint.ReleaseId ? sprint.ReleaseId : '';

    this.sprtForm = this.formBuilder.group({
      SprintNumber: [sprtNmb, [Validators.required, Validators.min(1)]],
      Goal: sprtGoal,
      StartDate: sprtStart,
      EndDate: sprtEnd,
      ReleaseId: sprtRel
    });
  }
}
