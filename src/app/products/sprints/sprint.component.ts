import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { Message, DataMessage } from '@cores/message.service';

import { Sprint, SprintService } from './services/sprint.service';

import { SprintEditComponent } from './sprint-edit.component';

@Component({
  templateUrl: './sprint.component.html'
})
export class SprintComponent implements OnInit {
  productId: string;
  sprintId: string;
  sprint: Sprint;
  hidden = true;

  constructor(
    public dialog: MatDialog,
    private readonly route: ActivatedRoute,
    private readonly sprintService: SprintService,
    private readonly router: Router
  ) { }

  ngOnInit() {
    this.productId = this.route.snapshot.paramMap.get('prodid');
    this.sprintId = this.route.snapshot.paramMap.get('id');
    this.getData();
  }

  onDelete(): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Sprint'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.sprintService.delete(this.productId, this.sprint) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.router.navigate(['/products', 'p', this.productId, 'sprints']);
      }
    });
  }

  onDisplay(): void {
    this.hidden = !this.hidden;
  }

  onEdit(): void {
    const dialogRef: MatDialogRef<SprintEditComponent> = this.dialog.open(
      SprintEditComponent,
      {
        width: '500px',
        data: {
          isNew: false,
          productId: this.productId,
          sprint: this.sprint
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  private getData(): void {
    this.sprintService.getById(this.productId, this.sprintId).subscribe(
      (sprint: DataMessage<Sprint>) => {
        if (sprint.success) {
          this.sprint = sprint.data;
        } else {
          this.router.navigate(['/products', 'p', this.productId, 'sprints']);
        }
      }
    );
  }
}
