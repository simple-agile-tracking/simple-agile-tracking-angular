import { NgModule } from '@angular/core';

import { SharedModule } from '@shares/shared.module';
import { SpinnerModule } from '@shares/spinner/spinner.module';

import { SprintComponent } from './sprint.component';
import { SprintEditComponent } from './sprint-edit.component';
import { SprintListComponent } from './sprint-list.component';
import { ReleaseDetailsComponent } from './release-details.component';

import { SprintStoriesComponent } from './stories/sprint-stories.component';
import { SprintStoryComponent } from './stories/sprint-story.component';
import { SprintStoryAddComponent } from './stories/sprint-story-add.component';
import { SprintStoryEditComponent } from './stories/sprint-story-edit.component';
import { SprintStoryIssueListComponent } from './stories/sprint-story-issue-list.component';
import { SprintStoryIssueEditComponent } from './stories/sprint-story-issue-edit.component';
import { SprintStoryIssueViewComponent } from './stories/sprint-story-issue-view.component';

import { SprintRouting } from './sprints-routing.module';

@NgModule({
  imports: [
    SharedModule,
    SpinnerModule,
    SprintRouting
  ],
  declarations: [
    SprintListComponent,
    SprintEditComponent,
    SprintComponent,
    SprintStoriesComponent,
    SprintStoryComponent,
    SprintStoryAddComponent,
    SprintStoryEditComponent,
    SprintStoryIssueListComponent,
    SprintStoryIssueEditComponent,
    SprintStoryIssueViewComponent,
    ReleaseDetailsComponent
  ],
  entryComponents: [
    SprintEditComponent,
    SprintStoryComponent,
    SprintStoryAddComponent,
    SprintStoryEditComponent,
    SprintStoryIssueEditComponent,
    SprintStoryIssueViewComponent
  ]
})
export class SprintsModule { }
