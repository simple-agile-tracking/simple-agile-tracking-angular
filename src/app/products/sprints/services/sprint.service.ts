import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@envs/environment';

import { LogService } from '@cores/log.service';
import { Message, MessageService, DataMessage } from '@cores/message.service';

const server = environment.server;

export interface Sprint {
  Id: string;
  ProductId: string;
  SprintNumber: number;
  Goal: string;
  StartDate: Date;
  EndDate: Date;
  ReleaseId: string;
  ReleaseNumber: number;
  ReleaseDate: Date;
  DateCreated: Date;
  OwnerId: string;
  LastModified: Date;
}

@Injectable({
  providedIn: 'root'
})
export class SprintService {
  constructor(private readonly http: HttpClient, private readonly logger: LogService, private readonly msgService: MessageService) { }

  getAll(productID: string): Observable<Sprint[]> {
    this.logger.log('sprint.service/getAll');

    return this.http.get<Sprint[]>(server + '/api/app/sprints/' + productID);
  }

  getById(productID: string, sprintID: string): Observable<DataMessage<Sprint>> {
    this.logger.log('sprint.service/getById');

    return this.msgService.getHTTP<Sprint>(
      this.http.get<Sprint>(server + '/api/app/sprints/' + productID + '/' + sprintID)
    );
  }

  delete(productID: string, sprint: Sprint): Observable<Message> {
    this.logger.log('sprint.service/delete');

    return this.msgService.fromHTTP(
      this.http.delete(
        server + '/api/app/sprints/admin/' + productID + '/' + sprint.Id
      ),
      'Sprint deleted successfully.',
      'Error Deleting Sprint'
    );
  }

  insert(productID: string, sprint: Sprint): Observable<Message> {
    this.logger.log('sprint.service/insert');

    return this.msgService.fromHTTP(
      this.http.post(
        server + '/api/app/sprints/admin/' + productID,
        JSON.stringify(sprint)
      ),
      'Sprint created successfully.',
      'Error Saving Sprint'
    );
  }

  update(productID: string, oldSprint: Sprint, newSprint: Sprint): Observable<Message> {
    this.logger.log('sprint.service/update');
    newSprint.Id = oldSprint.Id;

    return this.msgService.fromHTTP(
      this.http.put(
        server + '/api/app/sprints/admin/' + productID + '/' + newSprint.Id,
        JSON.stringify(newSprint)
      ),
      'Sprint updated successfully.',
      'Error Saving Sprint'
    );
  }
}
