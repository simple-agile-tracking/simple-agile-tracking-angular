import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { AuthUser, Issue } from './services/story-issue.service';

@Component({
  templateUrl: './sprint-story-issue-view.component.html'
})
export class SprintStoryIssueViewComponent implements OnInit {
  issue: Issue;
  assignedUser = 'n/a';

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<SprintStoryIssueViewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { issue: Issue, users: AuthUser[] }
  ) { }

  ngOnInit() {
    this.issue = this.data.issue;
    if (this.issue.AssignedTo && !!this.issue.AssignedTo.length) {
      this.assignedUser = this.data.users.filter(
        (u: AuthUser) => u.uid === this.issue.AssignedTo
      )[0].displayName;
    }
  }

  onEdit(): void {
    this.dialogRef.close('edit');
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
