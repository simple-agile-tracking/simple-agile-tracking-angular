import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@envs/environment';

import { LogService } from '@cores/log.service';
import { Message, MessageService, DataMessage } from '@cores/message.service';

import { Issue } from './story-issue.service';
import { Story } from '../../../backlog/stories/services/story.service';

const server = environment.server;

export interface SprintStory {
  Story: Story;
  Issues: Issue[];
}

@Injectable({
  providedIn: 'root'
})
export class SprintStoryService {
  constructor(private readonly http: HttpClient, private readonly logger: LogService, private readonly msgService: MessageService) { }

  getByIdForSprint(productID: string, storyID: string): Observable<DataMessage<SprintStory>> {
    this.logger.log('sprint-story.service/getByIdForSprint');

    return this.msgService.getHTTP<SprintStory>(
      this.http.get<SprintStory>(server + '/api/app/stories/' + productID + '/' + storyID)
    );
  }

  getBySprint(productID: string, sprintID: string): Observable<Story[]> {
    this.logger.log('sprint-story.service/getBySprint');

    return this.http.get<Story[]>(server + '/api/app/stories/' + productID + '/sprint/' + sprintID);
  }

  getFree(productID: string): Observable<Story[]> {
    this.logger.log('sprint-story.service/getFree');

    return this.http.get<Story[]>(server + '/api/app/stories/' + productID + '/free');
  }

  delete(productID: string, sprintID: string, story: Story): Observable<Message> {
    this.logger.log('sprint-story.service/delete');

    return this.msgService.fromHTTP(
      this.http.delete(
        server + '/api/app/sprints/admin/' + productID + '/' + sprintID + '/rem/' + story.Id
      ),
      'Sprint story removed successfully.',
      'Error Removing Story'
    );
  }

  insert(productID: string, sprintID: string, story: Story): Observable<Message> {
    this.logger.log('sprint-story.service/insert');

    return this.msgService.fromHTTP(
      this.http.post(
        server + '/api/app/sprints/admin/' + productID + '/' + sprintID,
        JSON.stringify(story)
      ),
      'Sprint story added successfully.',
      'Error Adding Story'
    );
  }

  update(productID: string, sprintID: string, oldStory: Story, newStory: Story): Observable<Message> {
    this.logger.log('sprint-story.service/update');
    newStory.Id = oldStory.Id;

    return this.msgService.fromHTTP(
      this.http.put(
        server + '/api/app/stories/admin/' + productID + '/' + sprintID + '/story/' + newStory.Id,
        JSON.stringify(newStory)
      ),
      'Sprint story updated successfully.',
      'Error Updating Story'
    );
  }
}
