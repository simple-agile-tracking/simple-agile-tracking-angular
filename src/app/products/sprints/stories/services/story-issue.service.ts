import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@envs/environment';

import { LogService } from '@cores/log.service';
import { Message, MessageService } from '@cores/message.service';

const server = environment.server;

export interface Issue {
  Id: string;
  StoryId: string;
  Name: string;
  Description: string;
  AssignedTo: string;
  AssignedName: string;
  Status: string;
  DateCreated: Date;
  OwnerId: string;
  LastModified: Date;
}

export interface AuthUser {
  uid: string;
  displayName: string;
  email: string;
  photoURL: string;
}

@Injectable({
  providedIn: 'root'
})
export class StoryIssueService {
  private readonly statusList = ['Pending', 'WIP', 'QA', 'Done'];

  constructor(private readonly http: HttpClient, private readonly logger: LogService, private readonly msgService: MessageService) { }

  getStatusList(): string[] {
    return this.statusList;
  }

  getUser(uid: string): Observable<AuthUser> {
    this.logger.log('story-issue.service/getUser');

    return this.http.get<AuthUser>(server + '/api/app/stories/admin/user/' + uid);
  }

  getUsers(): Observable<AuthUser[]> {
    this.logger.log('story-issue.service/getUsers');

    return this.http.get<AuthUser[]>(server + '/api/app/stories/admin/users');
  }

  delete(productID: string, storyID: string, issue: Issue): Observable<Message> {
    this.logger.log('story-issue.service/delete');

    return this.msgService.fromHTTP(
      this.http.delete(
        server + '/api/app/stories/admin/' + productID + '/' + storyID + '/issue/' + issue.Id
      ),
      'Sprint story issue deleted successfully.',
      'Error Deleting Issue'
    );
  }

  insert(productID: string, storyID: string, issue: Issue): Observable<Message> {
    this.logger.log('story-issue.service/insert');

    return this.msgService.fromHTTP(
      this.http.post(
        server + '/api/app/stories/admin/' + productID + '/' + storyID + '/issue',
        JSON.stringify(issue)
      ),
      'Sprint story issue added successfully.',
      'Error Saving Issue'
    );
  }

  update(productID: string, storyID: string, oldIssue: Issue, newIssue: Issue): Observable<Message> {
    this.logger.log('story-issue.service/update');
    newIssue.Id = oldIssue.Id;

    return this.msgService.fromHTTP(
      this.http.put(
        server + '/api/app/stories/admin/' + productID + '/' + storyID + '/issue/' + newIssue.Id,
        JSON.stringify(newIssue)
      ),
      'Sprint story issue updated successfully.',
      'Error Saving Issue'
    );
  }
}
