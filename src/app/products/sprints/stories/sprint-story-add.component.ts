import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Message } from '@cores/message.service';

import { Story } from '../../backlog/stories/services/story.service';

import { SprintStoryService } from './services/sprint-story.service';

@Component({
  templateUrl: './sprint-story-add.component.html'
})
export class SprintStoryAddComponent implements OnInit {
  styForm: FormGroup;
  stories: Story[] = [];
  hasStories = false;
  private productId: string;
  private sprintId: string;

  constructor(
    public dialogRef: MatDialogRef<SprintStoryAddComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { productId: string, sprintId: string },
    private readonly sprintStoryService: SprintStoryService,
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.productId = this.data.productId;
    this.sprintId = this.data.sprintId;

    this.initForm();

    this.sprintStoryService.getFree(this.productId).subscribe(
      (stories: Story[]) => {
        this.stories = stories;
        this.hasStories = !!stories.length;

        this.initForm();
      }
    );
  }

  get StoryId() { return this.styForm.get('StoryId'); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const newStory = this.styForm.value;

    this.sprintStoryService.insert(this.productId, this.sprintId, newStory).subscribe((rtn: Message) => {
      if (rtn.success) {
        this.dialogRef.close('ok');
      }
    });
  }

  private initForm(): void {
    this.styForm = this.formBuilder.group({
      StoryId: ['', Validators.required]
    });
  }
}
