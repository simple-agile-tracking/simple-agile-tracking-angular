import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Message } from '@cores/message.service';

import { Story } from '../../backlog/stories/services/story.service';

import { SprintStoryService } from './services/sprint-story.service';

@Component({
  templateUrl: './sprint-story-edit.component.html'
})
export class SprintStoryEditComponent implements OnInit {
  storyForm: FormGroup;
  storySizes = [1, 2, 3, 5, 8, 13, 21, 34];
  private productId: string;
  private sprintId: string;
  private story: Story;

  constructor(
    public dialogRef: MatDialogRef<SprintStoryEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { productId: string, sprintId: string, story: Story },
    private readonly sprintStoryService: SprintStoryService,
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.productId = this.data.productId;
    this.sprintId = this.data.sprintId;
    this.story = this.data.story;
    this.initForm(this.story);
  }

  get Name() { return this.storyForm.get('Name'); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const newStory = this.storyForm.value;

    this.sprintStoryService.update(this.productId, this.sprintId, this.story, newStory).subscribe((rtn: Message) => {
      if (rtn.success) {
        this.dialogRef.close('ok');
      }
    });
  }

  private initForm(story: Story): void {
    const storyPri = story ? story.Priority : null;
    const storyTtl = story ? story.Name : '';
    const storyDesc = story ? story.Description : '';
    const accp = story ? story.Acceptance : '';
    const risks = story ? story.RisksDependencies : '';
    const size = story && story.StorySize ? story.StorySize.toString() : '';
    const comp = story ? story.IsComplete : false;
    const qa = story ? story.QANotes : '';

    this.storyForm = this.formBuilder.group({
      Name: [storyTtl, Validators.required],
      Description: storyDesc,
      Acceptance: accp,
      RisksDependencies: risks,
      StorySize: size,
      Priority: storyPri,
      IsComplete: comp,
      QANotes: qa
    });
  }
}
