import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { Message } from '@cores/message.service';

import { Story } from '../../backlog/stories/services/story.service';
import { SprintStoryService } from './services/sprint-story.service';

import { SprintStoryComponent } from './sprint-story.component';
import { SprintStoryAddComponent } from './sprint-story-add.component';
import { SprintStoryEditComponent } from './sprint-story-edit.component';

@Component({
  selector: 'sat-sprint-stories',
  templateUrl: './sprint-stories.component.html'
})
export class SprintStoriesComponent implements OnInit {
  @ViewChild(MatTable, { static: true }) table: MatTable<Story>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  @Input() productId: string;
  @Input() sprintId: string;

  hasStories = false;
  displayedColumns = ['Name', 'Description', 'StorySize', 'IsComplete', 'Actions'];
  showSpinner = false;
  private storyData: MatTableDataSource<Story>;

  constructor(
    public dialog: MatDialog,
    private readonly sprintStoryService: SprintStoryService
  ) { }

  ngOnInit() {
    this.getData();
  }

  applyFilter(filterValue: string): void {
    this.storyData.filter = filterValue.trim().toLowerCase();
  }

  onAdd(): void {
    const dialogRef: MatDialogRef<SprintStoryAddComponent> = this.dialog.open(
      SprintStoryAddComponent,
      {
        width: '500px',
        data: {
          productId: this.productId,
          sprintId: this.sprintId
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onEdit(story: Story): void {
    const dialogRef: MatDialogRef<SprintStoryEditComponent> = this.dialog.open(
      SprintStoryEditComponent,
      {
        width: '800px',
        data: {
          productId: this.productId,
          sprintId: this.sprintId,
          story
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onRemove(story: Story): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Story',
          remType: 'Remove'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.sprintStoryService.delete(this.productId, this.sprintId, story) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.getData();
      }
    });
  }

  onView(story: Story): void {
    const dialogRef: MatDialogRef<SprintStoryComponent> = this.dialog.open(
      SprintStoryComponent,
      {
        width: '1000px',
        data: {
          productId: this.productId,
          storyId: story.Id
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'edit') {
        this.onEdit(story);
      }
    });
  }

  private getData(): void {
    this.showSpinner = true;
    this.sprintStoryService.getBySprint(this.productId, this.sprintId).subscribe(
      (stories: Story[]) => {
        this.hasStories = !!stories.length;
        this.storyData = new MatTableDataSource(stories);
        this.storyData.sort = this.sort;
        this.table.dataSource = this.storyData;
        this.showSpinner = false;
      },
      () => {
        this.hasStories = false;
        this.showSpinner = false;
      }
    );
  }
}
