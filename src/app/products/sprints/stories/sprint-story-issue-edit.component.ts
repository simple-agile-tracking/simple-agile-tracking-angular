import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { IdService } from '@cores/id.service';
import { Message } from '@cores/message.service';

import { AuthUser, Issue, StoryIssueService } from './services/story-issue.service';

@Component({
  templateUrl: './sprint-story-issue-edit.component.html'
})
export class SprintStoryIssueEditComponent implements OnInit {
  issForm: FormGroup;
  status: string[];
  users: AuthUser[] = [];
  isNew: boolean;
  title: string;
  private productId: string;
  private itemId: string;
  private issue: Issue;

  constructor(
    public dialogRef: MatDialogRef<SprintStoryIssueEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      isNew: boolean,
      productId: string,
      itemId: string,
      users: AuthUser[],
      issue: Issue
    },
    private readonly storyIssueService: StoryIssueService,
    private readonly formBuilder: FormBuilder,
    private readonly idService: IdService
  ) { }

  ngOnInit() {
    this.initForm(null);
    this.productId = this.data.productId;
    this.itemId = this.data.itemId;
    this.isNew = this.data.isNew;
    this.title = (this.isNew ? 'Add' : 'Edit') + ' Issue';
    this.users = this.data.users;
    this.status = this.storyIssueService.getStatusList();
    if (this.data.issue) {
      this.issue = this.data.issue;
      this.initForm(this.issue);
    }
  }

  get Name() { return this.issForm.get('Name'); }
  get Description() { return this.issForm.get('Description'); }
  get Status() { return this.issForm.get('Status'); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const newIssue = this.issForm.value;
    let saver: Observable<Message>;

    if (this.isNew) {
      newIssue.Id = this.idService.newID();
      saver = this.storyIssueService.insert(this.productId, this.itemId, newIssue);
    } else {
      saver = this.storyIssueService.update(this.productId, this.itemId, this.issue, newIssue);
    }

    saver.subscribe((rtn: Message) => {
      if (rtn.success) {
        this.dialogRef.close('ok');
      }
    });
  }

  private initForm(issue: Issue): void {
    const nm = issue ? issue.Name : '';
    const desc = issue ? issue.Description : '';
    const ato = issue ? issue.AssignedTo : null;
    const status = issue ? issue.Status : '';

    this.issForm = this.formBuilder.group({
      Name: [nm, Validators.required],
      Description: [desc, Validators.required],
      AssignedTo: ato,
      Status: [status, Validators.required]
    });
  }
}
