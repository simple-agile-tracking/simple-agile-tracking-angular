import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { DataMessage } from '@cores/message.service';

import { Story } from '../../backlog/stories/services/story.service';

import { SprintStory, SprintStoryService } from './services/sprint-story.service';

@Component({
  templateUrl: './sprint-story.component.html'
})
export class SprintStoryComponent implements OnInit {
  productId: string;
  storyId: string;
  story: Story;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<SprintStoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { productId: string, storyId: string },
    private readonly sprintStoryService: SprintStoryService
  ) { }

  ngOnInit() {
    this.productId = this.data.productId;
    this.storyId = this.data.storyId;

    this.sprintStoryService.getByIdForSprint(this.productId, this.storyId).subscribe(
      (ss: DataMessage<SprintStory>) => this.story = ss.success ? ss.data.Story : null
    );
  }

  onEdit(): void {
    this.dialogRef.close('edit');
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
