import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { of, forkJoin } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { Message } from '@cores/message.service';

import { SprintStoryService } from './services/sprint-story.service';
import { AuthUser, Issue, StoryIssueService } from './services/story-issue.service';

import { SprintStoryIssueEditComponent } from './sprint-story-issue-edit.component';
import { SprintStoryIssueViewComponent } from './sprint-story-issue-view.component';

@Component({
  selector: 'sat-story-issue-list',
  templateUrl: './sprint-story-issue-list.component.html'
})
export class SprintStoryIssueListComponent implements OnInit {
  @ViewChild(MatTable, { static: true }) table: MatTable<Issue>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  @Input() productId: string;
  @Input() storyId: string;

  hasIssues = false;
  displayedColumns = ['Name', 'AssignedName', 'Status', 'Actions'];
  users: AuthUser[] = [];
  showSpinner = false;
  private issueData: MatTableDataSource<Issue>;

  constructor(
    public dialog: MatDialog,
    private readonly sprintStoryService: SprintStoryService,
    private readonly storyIssueService: StoryIssueService
  ) { }

  ngOnInit() {
    this.getData();
  }

  applyFilter(filterValue: string): void {
    this.issueData.filter = filterValue.trim().toLowerCase();
  }

  onAdd(): void {
    const dialogRef: MatDialogRef<SprintStoryIssueEditComponent> = this.dialog.open(
      SprintStoryIssueEditComponent,
      {
        width: '800px',
        data: {
          isNew: true,
          productId: this.productId,
          itemId: this.storyId,
          users: this.users
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onDelete(issue: Issue): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Issue'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.storyIssueService.delete(this.productId, this.storyId, issue) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.getData();
      }
    });
  }

  onEdit(issue: Issue): void {
    const dialogRef: MatDialogRef<SprintStoryIssueEditComponent> = this.dialog.open(
      SprintStoryIssueEditComponent,
      {
        width: '800px',
        data: {
          isNew: false,
          productId: this.productId,
          itemId: this.storyId,
          users: this.users,
          issue
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onView(issue: Issue): void {
    const dialogRef: MatDialogRef<SprintStoryIssueViewComponent> = this.dialog.open(
      SprintStoryIssueViewComponent,
      {
        width: '800px',
        data: {
          issue,
          users: this.users
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'edit') {
        this.onEdit(issue);
      }
    });
  }

  private getData(): void {
    this.showSpinner = true;
    forkJoin([
      this.storyIssueService.getUsers(),
      this.sprintStoryService.getByIdForSprint(this.productId, this.storyId)
    ]).subscribe(
      ([users, ss]) => {
        if (ss.success) {
          this.users = users;
          this.hasIssues = !!ss.data.Issues.length;
          const issues = ss.data.Issues.map(
            (i: Issue, idx: number) => {
              ss.data.Issues[idx].AssignedName = this.getUser(i.AssignedTo);
              return ss.data.Issues[idx];
            }
          );
          this.issueData = new MatTableDataSource(issues);
          this.issueData.sort = this.sort;
          this.table.dataSource = this.issueData;
        } else {
          this.hasIssues = false;
        }

        this.showSpinner = false;
      }
    );
  }

  private getUser(uid: string): string {
    if (uid && !!uid.length) {
      return this.users.filter((u: AuthUser) => u.uid === uid)[0].displayName;
    }

    return 'n/a';
  }
}
