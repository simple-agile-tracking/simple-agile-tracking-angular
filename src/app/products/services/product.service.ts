import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@envs/environment';

import { LogService } from '@cores/log.service';
import { Message, MessageService, DataMessage } from '@cores/message.service';

const server = environment.server;

export interface Product {
  Id: string;
  AuthGroupId: string;
  Name: string;
  Description: string;
  SprintLength: number;
  InBacklogPhase: boolean;
  DateCreated: Date;
  OwnerId: string;
  LastModified: Date;
}

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private readonly http: HttpClient, private readonly logger: LogService, private readonly msgService: MessageService) { }

  getAll(): Observable<Product[]> {
    this.logger.log('product.service/getAll');

    return this.http.get<Product[]>(server + '/api/app/start');
  }

  getById(productId: string): Observable<DataMessage<Product>> {
    this.logger.log('product.service/getById');

    return this.msgService.getHTTP<Product>(
      this.http.get<Product>(server + '/api/app/products/' + productId)
    );
  }

  delete(productId: string): Observable<Message> {
    this.logger.log('product.service/delete');

    return this.msgService.fromHTTP(
      this.http.delete(
        server + '/api/app/products/admin/' + productId
      ),
      'Product deleted successfully.',
      'Error Deleting Product'
    );
  }

  insert(product: Product): Observable<Message> {
    this.logger.log('product.service/insert');

    return this.msgService.fromHTTP(
      this.http.post(
        server + '/api/app/products/admin',
        JSON.stringify(product)
      ),
      'Product created successfully.',
      'Error Saving Product'
    );
  }

  update(oldProduct: Product, newProduct: Product): Observable<Message> {
    this.logger.log('product.service/update');
    newProduct.Id = oldProduct.Id;

    return this.msgService.fromHTTP(
      this.http.put(
        server + '/api/app/products/admin/' + newProduct.Id,
        JSON.stringify(newProduct)
      ),
      'Product updated successfully.',
      'Error Saving Product'
    );
  }
}
