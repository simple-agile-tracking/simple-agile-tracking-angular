import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '@cores/auth-guard.service';

import { ProductListComponent } from './views/product-list.component';

export const ProductsRoutes: Routes = [
  { path: 'p', loadChildren: () => import('app/products/product.module').then(m => m.ProductModule) },
  { path: '', component: ProductListComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [
    RouterModule.forChild(ProductsRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProductsRouting { }
