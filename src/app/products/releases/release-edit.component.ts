import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { IdService } from '@cores/id.service';
import { Message } from '@cores/message.service';

import { Release, ReleaseService } from './services/release.service';

@Component({
  templateUrl: './release-edit.component.html'
})
export class ReleaseEditComponent implements OnInit {
  relForm: FormGroup;
  productId: string;
  isNew: boolean;
  title: string;
  private release: Release;

  constructor(
    public dialogRef: MatDialogRef<ReleaseEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { isNew: boolean, productId: string, release: Release },
    private readonly releaseService: ReleaseService,
    private readonly formBuilder: FormBuilder,
    private readonly idService: IdService
  ) { }

  ngOnInit() {
    this.productId = this.data.productId;
    this.isNew = this.data.isNew;
    this.title = (this.isNew ? 'Add' : 'Edit') + ' Release';
    if (this.data.release) {
      this.release = this.data.release;
    }
    this.initForm(this.release);
  }

  get ReleaseNumber() { return this.relForm.get('ReleaseNumber'); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const newRelease = this.relForm.value;
    let saver: Observable<Message>;

    if (this.isNew) {
      newRelease.Id = this.idService.newID();
      saver = this.releaseService.insert(this.productId, newRelease);
    } else {
      saver = this.releaseService.update(this.productId, this.release, newRelease);
    }

    saver.subscribe((rtn: Message) => {
      if (rtn.success) {
        this.dialogRef.close('ok');
      }
    });
  }

  private initForm(release: Release): void {
    const relCnt: number = release ? release.ReleaseNumber : null;
    const relDate: Date = release ? release.ReleaseDate : null;
    const relNotes = release ? release.Notes : '';

    this.relForm = this.formBuilder.group({
      ReleaseNumber: [relCnt, [Validators.required, Validators.min(1)]],
      ReleaseDate: relDate,
      Notes: relNotes
    });
  }
}
