import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Message } from '@cores/message.service';

import { ReleaseService } from './services/release.service';
import { Sprint } from '../sprints/services/sprint.service';

@Component({
  templateUrl: './release-sprint.component.html'
})
export class ReleaseSprintComponent implements OnInit {
  sptForm: FormGroup;
  sprints: Sprint[] = [];
  hasSprints = false;
  private productId: string;
  private releaseId: string;

  constructor(
    public dialogRef: MatDialogRef<ReleaseSprintComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { productId: string, releaseId: string },
    private readonly releaseService: ReleaseService,
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.productId = this.data.productId;
    this.releaseId = this.data.releaseId;
    this.releaseService.getFreeSprints(this.productId).subscribe(
      (sprints: Sprint[]) => {
        this.sprints = sprints;
        this.hasSprints = !!sprints.length;
      }
    );

    this.initForm();
  }

  get SprintId() { return this.sptForm.get('SprintId'); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const newSprint = this.sptForm.value;

    this.releaseService.addSprint(this.productId, this.releaseId, newSprint).subscribe((rtn: Message) => {
      if (rtn.success) {
        this.dialogRef.close();
      }
    });
  }

  private initForm(): void {
    this.sptForm = this.formBuilder.group({
      SprintId: ['', Validators.required]
    });
  }
}
