import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { Message } from '@cores/message.service';

import { Sprint } from '../sprints/services/sprint.service';
import { ReleaseService } from './services/release.service';

import { ReleaseSprintComponent } from './release-sprint.component';

@Component({
  selector: 'sat-release-sprints',
  templateUrl: './release-sprints.component.html'
})
export class ReleaseSprintsComponent implements OnInit {
  @ViewChild(MatTable, { static: true }) table: MatTable<Sprint>;

  @Input() productId: string;
  @Input() releaseId: string;

  hasSprints = false;
  displayedColumns = ['Sprint', 'Goal', 'Dates', 'Actions'];
  showSpinner = false;
  private relSprintsData: MatTableDataSource<Sprint>;

  constructor(
    public dialog: MatDialog,
    private readonly releaseService: ReleaseService
  ) { }

  ngOnInit() {
    this.getData();
  }

  applyFilter(filterValue: string): void {
    this.relSprintsData.filter = filterValue.trim().toLowerCase();
  }

  onAdd(): void {
    const dialogRef: MatDialogRef<ReleaseSprintComponent> = this.dialog.open(
      ReleaseSprintComponent,
      {
        width: '500px',
        data: {
          productId: this.productId,
          releaseId: this.releaseId
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onSprintRemove(sprint: Sprint): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Sprint',
          remType: 'Remove'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.releaseService.remSprint(this.productId, this.releaseId, sprint) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.getData();
      }
    });
  }

  private getData(): void {
    this.showSpinner = true;
    this.releaseService.getSprints(this.productId, this.releaseId).subscribe(
      (sprints: Sprint[]) => {
        this.hasSprints = !!sprints.length;
        this.relSprintsData = new MatTableDataSource(sprints);
        this.table.dataSource = this.relSprintsData;
        this.showSpinner = false;
      },
      () => {
        this.hasSprints = false;
        this.showSpinner = false;
      }
    );
  }
}
