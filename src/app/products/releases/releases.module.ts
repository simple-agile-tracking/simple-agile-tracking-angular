import { NgModule } from '@angular/core';

import { SharedModule } from '@shares/shared.module';
import { SpinnerModule } from '@shares/spinner/spinner.module';

import { ReleaseComponent } from './release.component';
import { ReleaseSprintsComponent } from './release-sprints.component';
import { ReleaseListComponent } from './release-list.component';
import { ReleaseEditComponent } from './release-edit.component';
import { ReleaseSprintComponent } from './release-sprint.component';

import { ReleaseRouting } from './releases-routing.module';

@NgModule({
  imports: [
    SharedModule,
    SpinnerModule,
    ReleaseRouting
  ],
  declarations: [
    ReleaseComponent,
    ReleaseSprintsComponent,
    ReleaseListComponent,
    ReleaseEditComponent,
    ReleaseSprintComponent
  ],
  entryComponents: [
    ReleaseEditComponent,
    ReleaseSprintComponent
  ]
})
export class ReleasesModule { }
