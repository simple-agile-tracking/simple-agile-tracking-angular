import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReleaseComponent } from './release.component';
import { ReleaseListComponent } from './release-list.component';

export const ReleaseRoutes: Routes = [
  { path: ':id', component: ReleaseComponent },
  { path: '', component: ReleaseListComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(ReleaseRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ReleaseRouting { }
