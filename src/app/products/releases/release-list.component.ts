import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { Message } from '@cores/message.service';

import { Release, ReleaseService } from './services/release.service';

import { ReleaseEditComponent } from './release-edit.component';

@Component({
  templateUrl: './release-list.component.html'
})
export class ReleaseListComponent implements OnInit {
  @ViewChild(MatTable, { static: true }) table: MatTable<Release>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  productId: string;
  hasReleases = false;
  displayedColumns = ['ReleaseNumber', 'ReleaseDate', 'Actions'];
  showSpinner = false;
  private relData: MatTableDataSource<Release>;

  constructor(
    public dialog: MatDialog,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly releaseService: ReleaseService
  ) { }

  ngOnInit() {
    this.productId = this.route.snapshot.paramMap.get('prodid');
    this.getData();
  }

  applyFilter(filterValue: string) {
    this.relData.filter = filterValue.trim().toLowerCase();
  }

  onAdd(): void {
    const dialogRef: MatDialogRef<ReleaseEditComponent> = this.dialog.open(
      ReleaseEditComponent,
      {
        width: '500px',
        data: {
          isNew: true,
          productId: this.productId
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onDelete(release: Release): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Release'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.releaseService.delete(this.productId, release) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.router.navigate(['/products', 'p', this.productId, 'releases']);
      }
    });
  }

  onEdit(release: Release): void {
    const dialogRef: MatDialogRef<ReleaseEditComponent> = this.dialog.open(
      ReleaseEditComponent,
      {
        width: '500px',
        data: {
          isNew: false,
          productId: this.productId,
          release
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  private getData(): void {
    this.showSpinner = true;
    this.releaseService.getAll(this.productId).subscribe(
      (releases: Release[]) => {
        this.hasReleases = !!releases.length;
        this.relData = new MatTableDataSource(releases);
        this.relData.sort = this.sort;
        this.table.dataSource = this.relData;
        this.showSpinner = false;
      },
      () => {
        this.hasReleases = false;
        this.showSpinner = false;
      }
    );
  }
}
