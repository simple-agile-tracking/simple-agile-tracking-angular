import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { Message, DataMessage } from '@cores/message.service';

import { Release, ReleaseService } from './services/release.service';

import { ReleaseEditComponent } from './release-edit.component';

@Component({
  templateUrl: './release.component.html'
})
export class ReleaseComponent implements OnInit {
  productId: string;
  releaseId: string;
  release: Release;
  hidden = true;

  constructor(
    public dialog: MatDialog,
    private readonly route: ActivatedRoute,
    private readonly releaseService: ReleaseService,
    private readonly router: Router
  ) { }

  ngOnInit() {
    this.productId = this.route.snapshot.paramMap.get('prodid');
    this.releaseId = this.route.snapshot.paramMap.get('id');
    this.getData();
  }

  onDelete(): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Release'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.releaseService.delete(this.productId, this.release) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.router.navigate(['/products', 'p', this.productId, 'releases']);
      }
    });
  }

  onDisplay(): void {
    this.hidden = !this.hidden;
  }

  onEdit(): void {
    const dialogRef: MatDialogRef<ReleaseEditComponent> = this.dialog.open(
      ReleaseEditComponent,
      {
        width: '500px',
        data: {
          isNew: false,
          productId: this.productId,
          release: this.release
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  private getData(): void {
    this.releaseService.getById(this.productId, this.releaseId).subscribe(
      (release: DataMessage<Release>) => {
        if (release.success) {
          this.release = release.data;
        } else {
          this.router.navigate(['/products', 'p', this.productId, 'releases']);
        }
      }
    );
  }
}
