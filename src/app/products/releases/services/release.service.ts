import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@envs/environment';

import { LogService } from '@cores/log.service';
import { Message, MessageService, DataMessage } from '@cores/message.service';

import { Sprint } from '../../sprints/services/sprint.service';

const server = environment.server;

export interface Release {
  Id: string;
  ProductId: string;
  ReleaseNumber: number;
  ReleaseDate: Date;
  Notes: string;
  DateCreated: Date;
  OwnerId: string;
  LastModified: Date;
}

@Injectable({
  providedIn: 'root'
})
export class ReleaseService {
  constructor(private readonly http: HttpClient, private readonly logger: LogService, private readonly msgService: MessageService) { }

  getAll(productID: string): Observable<Release[]> {
    this.logger.log('release.service/getAll');

    return this.http.get<Release[]>(server + '/api/app/releases/' + productID);
  }

  getById(productID: string, releaseID: string): Observable<DataMessage<Release>> {
    this.logger.log('release.service/getById');

    return this.msgService.getHTTP<Release>(
      this.http.get<Release>(server + '/api/app/releases/' + productID + '/' + releaseID)
    );
  }

  getFreeSprints(productID: string): Observable<Sprint[]> {
    this.logger.log('release.service/getFreeSprints');

    return this.http.get<Sprint[]>(server + '/api/app/sprints/' + productID + '/free');
  }

  getSprints(productID: string, releaseID: string): Observable<Sprint[]> {
    this.logger.log('release.service/getSprints');

    return this.http.get<Sprint[]>(server + '/api/app/sprints/' + productID + '/release/' + releaseID);
  }

  addSprint(productID: string, releaseID: string, sprint: Sprint): Observable<Message> {
    this.logger.log('release.service/addSprint');

    return this.msgService.fromHTTP(
      this.http.post(
        server + '/api/app/releases/admin/' + productID + '/' + releaseID,
        JSON.stringify(sprint)
      ),
      'Release sprint added successfully.',
      'Error Adding Sprint'
    );
  }

  delete(productID: string, release: Release): Observable<Message> {
    this.logger.log('release.service/delete');

    return this.msgService.fromHTTP(
      this.http.delete(
        server + '/api/app/releases/' + productID + '/' + release.Id
      ),
      'Release deleted successfully.',
      'Error Deleting Release'
    );
  }

  insert(productID: string, release: Release): Observable<Message> {
    this.logger.log('release.service/insert');

    return this.msgService.fromHTTP(
      this.http.post(
        server + '/api/app/releases/admin/' + productID,
        JSON.stringify(release)
      ),
      'Release created successfully.',
      'Error Saving Release'
    );
  }

  remSprint(productID: string, releaseID: string, sprint: Sprint): Observable<Message> {
    this.logger.log('release.service/remSprint');

    return this.msgService.fromHTTP(
      this.http.delete(
        server + '/api/app/releases/admin/' + productID + '/' + releaseID + '/rem/' + sprint.Id
      ),
      'Release sprint removed successfully.',
      'Error Removing Sprint'
    );
  }

  update(productID: string, oldRelease: Release, newRelease: Release): Observable<Message> {
    this.logger.log('release.service/update');
    newRelease.Id = oldRelease.Id;

    return this.msgService.fromHTTP(
      this.http.put(
        server + '/api/app/releases/admin/' + productID + '/' + newRelease.Id,
        JSON.stringify(newRelease)
      ),
      'Release updated successfully.',
      'Error Saving Release'
    );
  }
}
