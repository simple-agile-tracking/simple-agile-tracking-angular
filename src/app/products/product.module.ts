import { NgModule } from '@angular/core';

import { SharedModule } from '@shares/shared.module';
import { SpinnerModule } from '@shares/spinner/spinner.module';

import { ProductDetailComponent } from './views/product-detail.component';

import { BacklogComponent } from './backlog/backlog.component';
import { EpicComponent } from './backlog/epic.component';
import { EpicEditComponent } from './backlog/epic-edit.component';
import { EpicListComponent } from './backlog/epic-list.component';

import { EpicListResolver } from './backlog/services/epic-resolver.service';

import { FeaturesComponent } from './backlog/features/features.component';
import { FeatureComponent } from './backlog/features/feature.component';
import { FeatureEditComponent } from './backlog/features/feature-edit.component';
import { FeatureConvertComponent } from './backlog/features/feature-convert.component';

import { StoriesComponent } from './backlog/stories/stories.component';
import { StoryComponent } from './backlog/stories/story.component';
import { StoryEditComponent } from './backlog/stories/story-edit.component';
import { StoryConvertComponent } from './backlog/stories/story-convert.component';

import { ProductRouting } from './product-routing.module';

@NgModule({
  imports: [
    SharedModule,
    SpinnerModule,
    ProductRouting
  ],
  declarations: [
    ProductDetailComponent,
    BacklogComponent,
    EpicComponent,
    EpicEditComponent,
    EpicListComponent,
    FeaturesComponent,
    FeatureComponent,
    FeatureEditComponent,
    FeatureConvertComponent,
    StoriesComponent,
    StoryComponent,
    StoryEditComponent,
    StoryConvertComponent
  ],
  entryComponents: [
    EpicEditComponent,
    FeatureConvertComponent,
    FeatureEditComponent,
    StoryComponent,
    StoryConvertComponent,
    StoryEditComponent
  ],
  providers: [
    EpicListResolver
  ]
})
export class ProductModule { }
