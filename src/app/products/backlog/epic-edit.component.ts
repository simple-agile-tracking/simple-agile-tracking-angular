import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { IdService } from '@cores/id.service';
import { Message } from '@cores/message.service';

import { Epic, EpicService } from './services/epic.service';

@Component({
  templateUrl: './epic-edit.component.html'
})
export class EpicEditComponent implements OnInit {
  epicForm: FormGroup;
  epic: Epic;
  isNew: boolean;
  title: string;
  private productId: string;

  constructor(
    public dialogRef: MatDialogRef<EpicEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { isNew: boolean, productId: string, epic: Epic },
    private readonly epicService: EpicService,
    private readonly formBuilder: FormBuilder,
    private readonly idService: IdService
  ) { }

  ngOnInit() {
    this.productId = this.data.productId;
    this.isNew = this.data.isNew;
    this.title = (this.isNew ? 'Add' : 'Edit') + ' Epic';
    if (this.data.epic) {
      this.epic = this.data.epic;
    }
    this.initForm(this.epic);
  }

  get Title() { return this.epicForm.get('Title'); }
  get Description() { return this.epicForm.get('Description'); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const newEpic = this.epicForm.value;
    let saver: Observable<Message>;

    if (this.isNew) {
      newEpic.Id = this.idService.newID();
      saver = this.epicService.insert(this.productId, newEpic);
    } else {
      saver = this.epicService.update(this.productId, this.epic, newEpic);
    }

    saver.subscribe((rtn: Message) => {
      if (rtn.success) {
        this.dialogRef.close('ok');
      }
    });
  }

  private initForm(epic: Epic): void {
    const epicTtl = epic ? epic.Title : '';
    const epicDesc = epic ? epic.Description : '';
    const epicPri: number = epic ? epic.Priority : null;

    this.epicForm = this.formBuilder.group({
      Title: [epicTtl, Validators.required],
      Description: [epicDesc, Validators.required],
      Priority: epicPri
    });
  }
}
