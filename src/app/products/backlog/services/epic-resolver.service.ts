import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Epic, EpicService } from './epic.service';

@Injectable()
export class EpicListResolver implements Resolve<Epic[]> {
  constructor(private readonly epicService: EpicService, private readonly router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Epic[]> {
    const productId = route.parent.paramMap.get('prodid');

    return this.epicService.getAll(productId).pipe(
      map((epics: Epic[]) => {
        if (!epics) {
          this.router.navigate(['/products']);
        } else {
          return epics;
        }
        return null;
      })
    );
  }
}
