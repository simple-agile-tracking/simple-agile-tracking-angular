import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@envs/environment';

import { LogService } from '@cores/log.service';
import { Message, MessageService, DataMessage } from '@cores/message.service';

const server = environment.server;

export interface Epic {
  Id: string;
  ProductId: string;
  Title: string;
  Description: string;
  Priority: number;
  DateCreated: Date;
  OwnerId: string;
  LastModified: Date;
}

@Injectable({
  providedIn: 'root'
})
export class EpicService {
  constructor(private readonly http: HttpClient, private readonly logger: LogService, private readonly msgService: MessageService) { }

  getAll(productID: string): Observable<Epic[]> {
    this.logger.log('epic.service/getAll');

    return this.http.get<Epic[]>(server + '/api/app/epics/' + productID);
  }

  getById(productID: string, epicID: string): Observable<DataMessage<Epic>> {
    this.logger.log('epic.service/getById');

    return this.msgService.getHTTP<Epic>(
      this.http.get<Epic>(server + '/api/app/epics/' + productID + '/' + epicID)
    );
  }

  getExcel(productId: string): Observable<Blob> {
    return this.http.get<Blob>(
      server + '/api/app/epics/' + productId + '/excel',
      { responseType: 'blob' as 'json' }
    );
  }

  getFullList(productID: string): Observable<Epic[]> {
    this.logger.log('epic.service/getFullList');

    return this.http.get<Epic[]>(server + '/api/app/epics/' + productID + '/list');
  }

  delete(productID: string, epic: Epic): Observable<Message> {
    this.logger.log('epic.service/delete');

    return this.msgService.fromHTTP(
      this.http.delete(
        server + '/api/app/epics/admin/' + productID + '/' + epic.Id
      ),
      'Epic deleted successfully.',
      'Error Deleting Epic'
    );
  }

  insert(productID: string, epic: Epic): Observable<Message> {
    this.logger.log('epic.service/insert');

    return this.msgService.fromHTTP(
      this.http.post(
        server + '/api/app/epics/admin/' + productID,
        JSON.stringify(epic)
      ),
      'Epic created successfully.',
      'Error Saving Epic'
    );
  }

  update(productID: string, oldEpic: Epic, newEpic: Epic): Observable<Message> {
    this.logger.log('epic.service/update');
    newEpic.Id = oldEpic.Id;

    return this.msgService.fromHTTP(
      this.http.put(
        server + '/api/app/epics/admin/' + productID + '/' + newEpic.Id,
        JSON.stringify(newEpic)
      ),
      'Epic updated successfully.',
      'Error Saving Epic'
    );
  }
}
