import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { Message, DataMessage } from '@cores/message.service';

import { Epic, EpicService } from './services/epic.service';

import { EpicEditComponent } from './epic-edit.component';

@Component({
  templateUrl: './epic.component.html'
})
export class EpicComponent implements OnInit {
  productId: string;
  epicId: string;
  epic: Epic;
  hidden = false;

  constructor(
    public dialog: MatDialog,
    private readonly route: ActivatedRoute,
    private readonly epicService: EpicService,
    private readonly router: Router
  ) { }

  ngOnInit() {
    this.productId = this.route.snapshot.paramMap.get('prodid');
    this.epicId = this.route.snapshot.paramMap.get('id');
    this.getData();
  }

  onDelete(): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Epic'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.epicService.delete(this.productId, this.epic) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.router.navigate(['/products', 'p', this.productId, 'epics']);
      }
    });
  }

  onDisplay(): void {
    this.hidden = !this.hidden;
  }

  onEdit(): void {
    const dialogRef: MatDialogRef<EpicEditComponent> = this.dialog.open(
      EpicEditComponent,
      {
        width: '500px',
        data: {
          isNew: false,
          productId: this.productId,
          epic: this.epic
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  private getData(): void {
    this.epicService.getById(this.productId, this.epicId).subscribe(
      (epic: DataMessage<Epic>) => {
        if (epic.success) {
          this.epic = epic.data;
        } else {
          this.router.navigate(['/products', 'p', this.productId, 'epics']);
        }
      }
    );
  }
}
