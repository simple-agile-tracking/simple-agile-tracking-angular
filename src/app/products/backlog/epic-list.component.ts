import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { ExcelService } from '@cores/excel.service';
import { Message } from '@cores/message.service';

import { FeatureService } from './features/services/feature.service';

import { StoryService } from './stories/services/story.service';

import { Epic, EpicService } from './services/epic.service';

import { EpicEditComponent } from './epic-edit.component';

@Component({
  templateUrl: './epic-list.component.html'
})
export class EpicListComponent implements OnInit {
  @ViewChild(MatTable, { static: true }) table: MatTable<Epic>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  hasEpics = false;
  productId: string;
  displayedColumns = ['Priority', 'Title', 'Description', 'Actions'];
  showSpinner = false;
  private epicData: MatTableDataSource<Epic>;

  constructor(
    public dialog: MatDialog,
    private readonly route: ActivatedRoute,
    private readonly excelService: ExcelService,
    private readonly epicService: EpicService,
    private readonly featureService: FeatureService,
    private readonly storyService: StoryService
  ) { }

  ngOnInit() {
    const epics = this.route.snapshot.data.epics;
    this.hasEpics = !!epics.length;
    this.epicData = new MatTableDataSource(epics);
    this.epicData.sort = this.sort;
    this.table.dataSource = this.epicData;
    this.productId = this.route.snapshot.paramMap.get('prodid');
  }

  applyFilter(filterValue: string): void {
    this.epicData.filter = filterValue.trim().toLowerCase();
  }

  getBacklogExcel(): void {
    this.storyService.getBacklogExcel(this.productId).subscribe(
      (file: Blob) => this.excelService.download(file, 'backlogByEpic')
    );
  }

  getEpicExcel(): void {
    this.epicService.getExcel(this.productId).subscribe(
      (file: Blob) => this.excelService.download(file, 'epics')
    );
  }

  getFeatureExcel(): void {
    this.featureService.getExcel(this.productId).subscribe(
      (file: Blob) => this.excelService.download(file, 'features')
    );
  }

  getFeatureExcelByEpic(): void {
    this.featureService.getExcelByEpic(this.productId).subscribe(
      (file: Blob) => this.excelService.download(file, 'featuresByEpic')
    );
  }

  onAdd(): void {
    const dialogRef: MatDialogRef<EpicEditComponent> = this.dialog.open(
      EpicEditComponent,
      {
        width: '500px',
        data: {
          isNew: true,
          productId: this.productId
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onDelete(epic: Epic): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Epic'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.epicService.delete(this.productId, epic) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.getData();
      }
    });
  }

  onEdit(epic: Epic): void {
    const dialogRef: MatDialogRef<EpicEditComponent> = this.dialog.open(
      EpicEditComponent,
      {
        width: '500px',
        data: {
          isNew: false,
          productId: this.productId,
          epic
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  private getData(): void {
    this.showSpinner = true;
    this.epicService.getAll(this.productId).subscribe(
      (backlog: Epic[]) => {
        this.hasEpics = !!backlog.length;
        this.epicData = new MatTableDataSource(backlog);
        this.epicData.sort = this.sort;
        this.table.dataSource = this.epicData;
        this.showSpinner = false;
      },
      () => {
        this.hasEpics = false;
        this.showSpinner = false;
      }
    );
  }
}
