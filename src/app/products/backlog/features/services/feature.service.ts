import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@envs/environment';

import { LogService } from '@cores/log.service';
import { Message, MessageService, DataMessage } from '@cores/message.service';

const server = environment.server;

export interface Feature {
  Id: string;
  EpicId: string;
  Title: string;
  Description: string;
  RisksDependencies: string;
  Priority: number;
  DateCreated: Date;
  OwnerId: string;
  LastModified: Date;
}

@Injectable({
  providedIn: 'root'
})
export class FeatureService {
  constructor(private readonly http: HttpClient, private readonly logger: LogService, private readonly msgService: MessageService) { }

  getAll(productID: string, epicID: string): Observable<Feature[]> {
    this.logger.log('feature.service/getAll');

    return this.http.get<Feature[]>(server + '/api/app/features/' + productID + '/' + epicID);
  }

  getAllButOne(productID: string, epicID: string, featureID: string): Observable<Feature[]> {
    this.logger.log('feature.service/getAllButOne');

    const theseFeatures: Feature[] = [];

    return this.http.get<Feature[]>(
      server + '/api/app/features/' + productID + '/' + epicID + '/list'
    ).pipe(
      map(
        (data: Feature[]) => {
          for (const d of data) {
            if (d.Id !== featureID && d.EpicId === epicID) {
              theseFeatures.push(d);
            }
          }

          return theseFeatures;
        }
      )
    );
  }

  getById(productID: string, epicID: string, featureID: string): Observable<DataMessage<Feature>> {
    this.logger.log('feature.service/getById');

    return this.msgService.getHTTP<Feature>(
      this.http.get<Feature>(server + '/api/app/features/' + productID + '/' + epicID + '/' + featureID)
    );
  }

  getExcel(productId: string): Observable<Blob> {
    return this.http.get<Blob>(
      server + '/api/app/features/' + productId + '/excel',
      { responseType: 'blob' as 'json' }
    );
  }

  getExcelByEpic(productId: string): Observable<Blob> {
    return this.http.get<Blob>(
      server + '/api/app/features/' + productId + '/excelbyepic',
      { responseType: 'blob' as 'json' }
    );
  }

  getFullList(productID: string, epicID: string): Observable<Feature[]> {
    this.logger.log('feature.service/getFullList');

    return this.http.get<Feature[]>(server + '/api/app/features/' + productID + '/' + epicID + '/list');
  }

  convertToItem(productID: string, featureID: string, itemID: string, conversion: Feature): Observable<Message> {
    this.logger.log('feature.service/convertToItem');

    return this.msgService.fromHTTP(
      this.http.post(
        server + '/api/app/stories/admin/' + productID + '/' + featureID + '/convert/' + itemID,
        JSON.stringify(conversion)
      ),
      'Feature converted to item successfully.',
      'Error Converting to Item'
    );
  }

  delete(productID: string, epicID: string, feature: Feature): Observable<Message> {
    this.logger.log('feature.service/delete');

    return this.msgService.fromHTTP(
      this.http.delete(
        server + '/api/app/features/admin/' + productID + '/' + epicID + '/' + feature.Id
      ),
      'Feature deleted successfully.',
      'Error Deleting Feature'
    );
  }

  insert(productID: string, feature: Feature): Observable<Message> {
    this.logger.log('feature.service/insert');
    const epicID = feature.EpicId;

    return this.msgService.fromHTTP(
      this.http.post(
        server + '/api/app/features/admin/' + productID + '/' + epicID,
        JSON.stringify(feature)
      ),
      'Feature created successfully.',
      'Error Saving Feature'
    );
  }

  update(productID: string, epicID: string, oldFeature: Feature, newFeature: Feature): Observable<Message> {
    this.logger.log('feature.service/update');
    newFeature.Id = oldFeature.Id;

    return this.msgService.fromHTTP(
      this.http.put(
        server + '/api/app/features/admin/' + productID + '/' + epicID + '/' + newFeature.Id,
        JSON.stringify(newFeature)
      ),
      'Feature updated successfully.',
      'Error Saving Feature'
    );
  }
}
