import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { IdService } from '@cores/id.service';
import { Message } from '@cores/message.service';

import { Epic, EpicService } from '../services/epic.service';

import { Feature, FeatureService } from './services/feature.service';

@Component({
  templateUrl: './feature-edit.component.html'
})
export class FeatureEditComponent implements OnInit {
  featureForm: FormGroup;
  epicList: Epic[];
  isNew: boolean;
  title: string;
  private productId: string;
  private epicId: string;
  private feature: Feature;

  constructor(
    public dialogRef: MatDialogRef<FeatureEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { isNew: boolean, productId: string, epicId: string, feature: Feature },
    private readonly epicService: EpicService,
    private readonly featureService: FeatureService,
    private readonly formBuilder: FormBuilder,
    private readonly idService: IdService
  ) { }

  ngOnInit() {
    this.productId = this.data.productId;
    this.epicId = this.data.epicId;
    this.isNew = this.data.isNew;
    this.title = (this.isNew ? 'Add' : 'Edit') + ' Feature';
    if (this.data.feature) {
      this.feature = this.data.feature;
    }
    this.initForm(this.feature);
    this.epicService.getFullList(this.productId).subscribe(
      (epicList: Epic[]) => {
        this.epicList = epicList;
        this.initForm(this.feature);
      }
    );
  }

  get EpicId() { return this.featureForm.get('EpicId'); }
  get Title() { return this.featureForm.get('Title'); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const newFeature = this.featureForm.value;
    let saver: Observable<Message>;

    if (this.isNew) {
      newFeature.Id = this.idService.newID();
      saver = this.featureService.insert(this.productId, newFeature);
    } else {
      this.featureService.update(this.productId, this.epicId, this.feature, newFeature);
    }

    saver.subscribe((rtn: Message) => {
      if (rtn.success) {
        this.dialogRef.close('ok');
      }
    });
  }

  private initForm(feature: Feature): void {
    const featEpc = this.epicId;
    const featTtl = feature ? feature.Title : '';
    const featDesc = feature ? feature.Description : '';
    const featRisks = feature ? feature.RisksDependencies : '';
    const featPri = feature ? feature.Priority : null;

    this.featureForm = this.formBuilder.group({
      EpicId: [featEpc, Validators.required],
      Title: [featTtl, Validators.required],
      Description: featDesc,
      RisksDependencies: featRisks,
      Priority: featPri
    });
  }
}
