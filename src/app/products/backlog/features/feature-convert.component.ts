import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { forkJoin } from 'rxjs';

import { Message } from '@cores/message.service';

import { Feature, FeatureService } from './services/feature.service';

import { Sprint, SprintService } from '../../sprints/services/sprint.service';

@Component({
  templateUrl: './feature-convert.component.html'
})
export class FeatureConvertComponent implements OnInit {
  featCvtForm: FormGroup;
  feature: Feature;
  features: Feature[];
  sprints: Sprint[];
  private productId: string;
  private epicId: string;

  constructor(
    public dialogRef: MatDialogRef<FeatureConvertComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { productId: string, epicId: string, feature: Feature },
    private readonly sprintService: SprintService,
    private readonly featureService: FeatureService,
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.productId = this.data.productId;
    this.epicId = this.data.epicId;
    this.feature = this.data.feature;
    this.initForm(this.feature);
    forkJoin([
      this.featureService.getAllButOne(this.productId, this.epicId, this.feature.Id),
      this.sprintService.getAll(this.productId)
    ]).subscribe(
      ([features, sprints]) => {
        this.features = features;
        this.sprints = sprints;
        this.initForm(this.feature);
      }
    );
  }

  get FeatureId() { return this.featCvtForm.get('FeatureId'); }
  get Name() { return this.featCvtForm.get('Name'); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const conversion = this.featCvtForm.value;

    this.featureService.convertToItem(this.productId, conversion.FeatureId, this.feature.Id, conversion).subscribe(
      (rtn: Message) => {
        if (rtn.success) {
          this.dialogRef.close('ok');
        }
      }
    );
  }

  private initForm(feature: Feature): void {
    const featTtl = feature ? feature.Title : '';
    const featDesc = feature ? feature.Description : '';
    const risks = feature ? feature.RisksDependencies : '';

    this.featCvtForm = this.formBuilder.group({
      FeatureId: ['', Validators.required],
      Name: [featTtl, Validators.required],
      Description: featDesc,
      RisksDependencies: risks,
      Priority: null,
      SprintId: ''
    });
  }
}
