import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { Message } from '@cores/message.service';

import { Feature, FeatureService } from './services/feature.service';

import { FeatureConvertComponent } from './feature-convert.component';
import { FeatureEditComponent } from './feature-edit.component';

@Component({
  selector: 'sat-features',
  templateUrl: './features.component.html'
})
export class FeaturesComponent implements OnInit {
  @ViewChild(MatTable, { static: true }) table: MatTable<Feature>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  @Input() productId: string;
  @Input() epicId: string;

  hasFeatures = false;
  displayedColumns = ['Priority', 'Title', 'Description', 'Items', 'Actions'];
  showSpinner = false;
  private featureData: MatTableDataSource<Feature>;

  constructor(
    public dialog: MatDialog,
    private readonly featureService: FeatureService
  ) { }

  ngOnInit() {
    this.getData();
  }

  applyFilter(filterValue: string): void {
    this.featureData.filter = filterValue.trim().toLowerCase();
  }

  onAdd(): void {
    const dialogRef: MatDialogRef<FeatureEditComponent> = this.dialog.open(
      FeatureEditComponent,
      {
        width: '500px',
        data: {
          isNew: true,
          productId: this.productId,
          epicId: this.epicId
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onConvert(feature: Feature): void {
    const dialogRef: MatDialogRef<FeatureConvertComponent> = this.dialog.open(
      FeatureConvertComponent,
      {
        width: '500px',
        data: {
          productId: this.productId,
          epicId: this.epicId,
          feature
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onDelete(feature: Feature): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Feature'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.featureService.delete(this.productId, this.epicId, feature) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.getData();
      }
    });
  }

  onEdit(feature: Feature): void {
    const dialogRef: MatDialogRef<FeatureEditComponent> = this.dialog.open(
      FeatureEditComponent,
      {
        width: '500px',
        data: {
          isNew: false,
          productId: this.productId,
          epicId: this.epicId,
          feature
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  private getData(): void {
    this.showSpinner = true;
    this.featureService.getAll(this.productId, this.epicId).subscribe(
      (features: Feature[]) => {
        this.hasFeatures = !!features.length;
        this.featureData = new MatTableDataSource(features);
        this.featureData.sort = this.sort;
        this.table.dataSource = this.featureData;
        this.showSpinner = false;
      },
      () => {
        this.hasFeatures = false;
        this.showSpinner = false;
      }
    );
  }
}
