import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { Message } from '@cores/message.service';

import { Epic, EpicService } from '../services/epic.service';

import { Feature, FeatureService } from './services/feature.service';

import { FeatureEditComponent } from './feature-edit.component';

@Component({
  templateUrl: './feature.component.html'
})
export class FeatureComponent implements OnInit {
  productId: string;
  epicId: string;
  epic: Epic;
  featureId: string;
  feature: Feature;
  title = 'Feature';
  hidden = false;

  constructor(
    public dialog: MatDialog,
    private readonly route: ActivatedRoute,
    private readonly epicService: EpicService,
    private readonly featureService: FeatureService,
    private readonly router: Router
  ) { }

  ngOnInit() {
    this.productId = this.route.snapshot.paramMap.get('prodid');
    this.epicId = this.route.snapshot.paramMap.get('epicid');
    this.featureId = this.route.snapshot.paramMap.get('id');
    this.getData();
  }

  onDelete(): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Feature'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.featureService.delete(this.productId, this.epicId, this.feature) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.router.navigate(['/products', 'p', this.productId, 'epic', this.epicId]);
      }
    });
  }

  onDisplay(): void {
    this.hidden = !this.hidden;
  }

  onEdit(): void {
    const dialogRef: MatDialogRef<FeatureEditComponent> = this.dialog.open(
      FeatureEditComponent,
      {
        width: '500px',
        data: {
          isNew: false,
          productId: this.productId,
          epicId: this.epicId,
          feature: this.feature
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  private getData(): void {
    forkJoin([
      this.epicService.getById(this.productId, this.epicId),
      this.featureService.getById(this.productId, this.epicId, this.featureId)
    ]).subscribe(
      ([epic, feature]) => {
        if (epic.success && feature.success) {
          this.epic = epic.data;
          this.feature = feature.data;
          this.title = 'Epic #' + this.epic.Priority + '. ' + this.epic.Title + ' > ';
          this.title += 'Feature ' + (this.feature.Priority ? ' #' + this.feature.Priority + '. ' : ' ');
          this.title += this.feature.Title;
        } else {
          this.router.navigate(['/products', 'p', this.productId, 'epics', this.epicId]);
        }
      }
    );
  }
}
