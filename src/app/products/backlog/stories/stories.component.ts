import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { Message } from '@cores/message.service';

import { Story, StoryService } from './services/story.service';

import { StoryComponent } from './story.component';
import { StoryConvertComponent } from './story-convert.component';
import { StoryEditComponent } from './story-edit.component';

@Component({
  selector: 'sat-items',
  templateUrl: './stories.component.html'
})
export class StoriesComponent implements OnInit {
  @ViewChild(MatTable, { static: true }) table: MatTable<Story>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  @Input() productId: string;
  @Input() epicId: string;
  @Input() featureId: string;

  hasStories = false;
  displayedColumns = ['Priority', 'Name', 'Description', 'StorySize', 'SprintNumber', 'ReleaseNumber', 'Actions'];
  showSpinner = false;
  private itemData: MatTableDataSource<Story>;

  constructor(
    public dialog: MatDialog,
    private readonly storyService: StoryService
  ) { }

  ngOnInit() {
    this.getData();
  }

  applyFilter(filterValue: string): void {
    this.itemData.filter = filterValue.trim().toLowerCase();
  }

  onAdd(): void {
    const dialogRef: MatDialogRef<StoryEditComponent> = this.dialog.open(
      StoryEditComponent,
      {
        width: '500px',
        data: {
          isNew: true,
          productId: this.productId,
          epicId: this.epicId,
          featureId: this.featureId
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onConvert(story: Story): void {
    const dialogRef: MatDialogRef<StoryConvertComponent> = this.dialog.open(
      StoryConvertComponent,
      {
        width: '500px',
        data: {
          productId: this.productId,
          epicId: this.epicId,
          story
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onDelete(story: Story): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Item'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.storyService.delete(this.productId, story) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.getData();
      }
    });
  }

  onEdit(story: Story): void {
    const dialogRef: MatDialogRef<StoryEditComponent> = this.dialog.open(
      StoryEditComponent,
      {
        width: '500px',
        data: {
          isNew: false,
          productId: this.productId,
          epicId: this.epicId,
          featureId: this.featureId,
          story
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onView(story: Story): void {
    const dialogRef: MatDialogRef<StoryComponent> = this.dialog.open(
      StoryComponent,
      {
        width: '500px',
        data: {
          productId: this.productId,
          story
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'edit') {
        this.onEdit(story);
      } else if (result === 'delete') {
        this.getData();
      }
    });
  }

  private getData(): void {
    this.showSpinner = true;
    this.storyService.getByFeature(this.productId, this.epicId, this.featureId).subscribe(
      (stories: Story[]) => {
        this.hasStories = !!stories.length;
        this.itemData = new MatTableDataSource(stories);
        this.itemData.sort = this.sort;
        this.table.dataSource = this.itemData;
        this.showSpinner = false;
      },
      () => {
        this.hasStories = false;
        this.showSpinner = false;
      }
    );
  }
}
