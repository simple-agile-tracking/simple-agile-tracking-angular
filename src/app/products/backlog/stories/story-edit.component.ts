import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { forkJoin, Observable } from 'rxjs';

import { IdService } from '@cores/id.service';
import { Message } from '@cores/message.service';

import { Feature, FeatureService } from '../features/services/feature.service';

import { Story, StoryService } from './services/story.service';

import { Sprint, SprintService } from '../../sprints/services/sprint.service';

@Component({
  templateUrl: './story-edit.component.html'
})
export class StoryEditComponent implements OnInit {
  storyForm: FormGroup;
  features: Feature[];
  isNew: boolean;
  sprints: Sprint[];
  storySizes = [1, 2, 3, 5, 8, 13, 21, 34];
  title: string;
  private productId: string;
  private epicId: string;
  private featureId: string;
  private story: Story;

  constructor(
    public dialogRef: MatDialogRef<StoryEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { isNew: boolean, productId: string, epicId: string, featureId: string, story: Story },
    private readonly featureService: FeatureService,
    private readonly storyService: StoryService,
    private readonly sprintService: SprintService,
    private readonly formBuilder: FormBuilder,
    private readonly idService: IdService
  ) { }

  ngOnInit() {
    this.productId = this.data.productId;
    this.epicId = this.data.epicId;
    this.featureId = this.data.featureId;
    this.isNew = this.data.isNew;
    this.title = (this.isNew ? 'Add' : 'Edit') + ' Backlog Item';
    if (this.data.story) {
      this.story = this.data.story;
    }
    this.initForm(this.story);
    forkJoin([
      this.featureService.getFullList(this.productId, this.epicId),
      this.sprintService.getAll(this.productId)
    ]).subscribe(
      ([features, sprints]) => {
        this.features = features;
        this.sprints = sprints;
        this.initForm(this.story);
      }
    );
  }

  get Name() { return this.storyForm.get('Name'); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const newStory = this.storyForm.value;
    let saver: Observable<Message>;

    if (this.isNew) {
      newStory.Id = this.idService.newID();
      saver = this.storyService.insert(this.productId, this.epicId, this.featureId, newStory);
    } else {
      saver = this.storyService.update(this.productId, this.epicId, this.featureId, this.story, newStory);
    }

    saver.subscribe((rtn: Message) => {
      if (rtn.success) {
        this.dialogRef.close('ok');
      }
    });
  }

  private initForm(story: Story): void {
    const storyPri = story ? story.Priority : null;
    const storyTtl = story ? story.Name : '';
    const storyDesc = story ? story.Description : '';
    const accp = story ? story.Acceptance : '';
    const risks = story ? story.RisksDependencies : '';
    const size = story && story.StorySize ? story.StorySize.toString() : '';
    const storyFeat = story && story.FeatureId ? story.FeatureId : this.featureId;
    const sprtId = story && story.SprintId ? story.SprintId : '';
    const comp = story ? story.IsComplete : false;
    const qa = story ? story.QANotes : '';

    this.storyForm = this.formBuilder.group({
      Name: [storyTtl, Validators.required],
      Description: storyDesc,
      Acceptance: accp,
      RisksDependencies: risks,
      StorySize: size,
      FeatureId: storyFeat,
      Priority: storyPri,
      SprintId: sprtId,
      IsComplete: comp,
      QANotes: qa
    });
  }
}
