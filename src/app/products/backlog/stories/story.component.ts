import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { Message } from '@cores/message.service';

import { Story, StoryService } from './services/story.service';

@Component({
  templateUrl: './story.component.html'
})
export class StoryComponent implements OnInit {
  story: Story;
  title: string;
  private productId: string;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<StoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { productId: string, story: Story },
    private readonly storyService: StoryService
  ) { }

  ngOnInit() {
    this.productId = this.data.productId;
    this.story = this.data.story;
    this.title = 'Item' + (this.story.Priority ? ' #' + this.story.Priority : '') + ' ' + this.story.Name;
  }

  onDelete(): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Item'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.storyService.delete(this.productId, this.story) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.dialogRef.close('delete');
      }
    });
  }

  onEdit(): void {
    this.dialogRef.close('edit');
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
