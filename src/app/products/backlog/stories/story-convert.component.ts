import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Message } from '@cores/message.service';

import { Epic, EpicService } from '../services/epic.service';

import { Story, StoryService } from './services/story.service';

@Component({
  templateUrl: './story-convert.component.html'
})
export class StoryConvertComponent implements OnInit {
  convertForm: FormGroup;
  epicList: Epic[];
  story: Story;
  private productId: string;
  private epicId: string;

  constructor(
    public dialogRef: MatDialogRef<StoryConvertComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { productId: string, epicId: string, story: Story },
    private readonly epicService: EpicService,
    private readonly storyService: StoryService,
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.productId = this.data.productId;
    this.epicId = this.data.epicId;
    this.story = this.data.story;
    this.initForm(this.story);
    this.epicService.getFullList(this.productId).subscribe(
      (epicList: Epic[]) => {
        this.epicList = epicList;
        this.initForm(this.story);
      }
    );
  }

  get EpicId() { return this.convertForm.get('EpicId'); }
  get Title() { return this.convertForm.get('Title'); }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const conversion = this.convertForm.value;

    this.storyService.convertToFeature(this.productId, conversion.EpicId, this.story.Id, conversion).subscribe((rtn: Message) => {
      if (rtn.success) {
        this.dialogRef.close('ok');
      }
    });
  }

  private initForm(story: Story): void {
    const storyEpc = this.epicId;
    const storyTtl = story ? story.Name : '';
    const storyDesc = story ? story.Description : '';
    const risks = story ? story.RisksDependencies : '';

    this.convertForm = this.formBuilder.group({
      EpicId: [storyEpc, Validators.required],
      Title: [storyTtl, Validators.required],
      Description: storyDesc,
      RisksDependencies: risks,
      Priority: null
    });
  }
}
