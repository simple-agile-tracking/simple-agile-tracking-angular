import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@envs/environment';

import { LogService } from '@cores/log.service';
import { Message, MessageService } from '@cores/message.service';

const server = environment.server;

export interface Story {
  Id: string;
  ProductId: string;
  Priority: number;
  Name: string;
  Description: string;
  Acceptance: string;
  RisksDependencies: string;
  StorySize: number;
  FeatureId: string;
  EpicId: string;
  SprintId: string;
  SprintNumber: number;
  StartDate: Date;
  EndDate: Date;
  ReleaseId: string;
  ReleaseNumber: number;
  ReleaseDate: Date;
  IsComplete: boolean;
  QANotes: string;
  DateCreated: Date;
  OwnerId: string;
  LastModified: Date;
}

@Injectable({
  providedIn: 'root'
})
export class StoryService {
  constructor(private readonly http: HttpClient, private readonly logger: LogService, private readonly msgService: MessageService) { }

  getAll(productID: string): Observable<Story[]> {
    this.logger.log('story.service/getAll');

    return this.http.get<Story[]>(server + '/api/app/stories/' + productID);
  }

  getByFeature(productID: string, epicID: string, featureID: string): Observable<Story[]> {
    this.logger.log('story.service/getByFeature');

    return this.http.get<Story[]>(server + '/api/app/stories/' + productID + '/' + epicID + '/' + featureID);
  }

  getExcel(productId: string): Observable<Blob> {
    return this.http.get<Blob>(
      server + '/api/app/stories/' + productId + '/excel',
      { responseType: 'blob' as 'json' }
    );
  }

  getBacklogExcel(productId: string): Observable<Blob> {
    return this.http.get<Blob>(
      server + '/api/app/stories/' + productId + '/excelbyepic',
      { responseType: 'blob' as 'json' }
    );
  }

  convertToFeature(productID: string, epicID: string, storyID: string, conversion: Story): Observable<Message> {
    this.logger.log('story.service/convertToFeature');

    return this.msgService.fromHTTP(
      this.http.post(
        server + '/api/app/features/admin/' + productID + '/' + epicID + '/convert/' + storyID,
        JSON.stringify(conversion)
      ),
      'Backlog item converted to feature successfully.',
      'Error Converting to Feature'
    );
  }

  delete(productID: string, story: Story): Observable<Message> {
    this.logger.log('story.service/delete');

    return this.msgService.fromHTTP(
      this.http.delete(
        server + '/api/app/stories/admin/' + productID + '/' + story.Id
      ),
      'Backlog item deleted successfully.',
      'Error Deleting Item'
    );
  }

  insert(productID: string, epicID: string, featureID: string, story: Story): Observable<Message> {
    this.logger.log('story.service/insert');

    return this.msgService.fromHTTP(
      this.http.post(
        server + '/api/app/stories/admin/' + productID + '/' + epicID + '/' + featureID,
        JSON.stringify(story)
      ),
      'Backlog item created successfully.',
      'Error Saving Item'
    );
  }

  update(productID: string, epicID: string, featureID: string, oldStory: Story, newStory: Story): Observable<Message> {
    this.logger.log('story.service/update');
    newStory.Id = oldStory.Id;

    return this.msgService.fromHTTP(
      this.http.put(
        server + '/api/app/stories/admin/' + productID + '/' + epicID + '/' + featureID + '/' + newStory.Id,
        JSON.stringify(newStory)
      ),
      'Backlog item updated successfully.',
      'Error Saving Item'
    );
  }
}
