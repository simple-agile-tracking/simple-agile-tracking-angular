import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { ExcelService } from '@cores/excel.service';
import { Message } from '@cores/message.service';

import { Story, StoryService } from './stories/services/story.service';

import { StoryComponent } from './stories/story.component';
import { StoryEditComponent } from './stories/story-edit.component';

@Component({
  templateUrl: './backlog.component.html'
})
export class BacklogComponent implements OnInit {
  @ViewChild(MatTable, { static: true }) table: MatTable<Story>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  hasStories = false;
  productId: string;
  displayedColumns = [
    'Priority',
    'Name',
    'Description',
    'StorySize',
    'FeatureTitle',
    'SprintNumber',
    'ReleaseNumber',
    'IsComplete',
    'Actions'
  ];
  showSpinner = false;
  private storiesData: MatTableDataSource<Story>;
  private completeOnly = false;
  private stories: Story[];

  constructor(
    public dialog: MatDialog,
    private readonly route: ActivatedRoute,
    private readonly excelService: ExcelService,
    private readonly storyService: StoryService
  ) { }

  ngOnInit() {
    this.showSpinner = true;
    this.productId = this.route.snapshot.paramMap.get('prodid');
    this.getData();
  }

  applyComplete(): void {
    this.completeOnly = !this.completeOnly;
    const storyList = this.completeOnly ? this.stories.filter(s => !!s.IsComplete) : this.stories;
    this.storiesData = new MatTableDataSource(storyList);
    this.storiesData.paginator = this.paginator;
    this.storiesData.paginator.firstPage();
    this.storiesData.sort = this.sort;
    this.table.dataSource = this.storiesData;
  }

  applyFilter(filterValue: string): void {
    this.storiesData.filter = filterValue.trim().toLowerCase();
    if (this.storiesData.paginator) {
      this.storiesData.paginator.firstPage();
    }
  }

  getExcel(): void {
    this.storyService.getExcel(this.productId).subscribe(
      (file: Blob) => this.excelService.download(file, 'backlog')
    );
  }

  onAdd(): void {
    const dialogRef: MatDialogRef<StoryEditComponent> = this.dialog.open(
      StoryEditComponent,
      {
        width: '500px',
        data: {
          isNew: true,
          productId: this.productId
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onDelete(story: Story): void {
    const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
      DialogDeleteComponent,
      {
        width: '500px',
        data: {
          recordType: 'Item'
        }
      }
    );

    dialogRef.afterClosed().pipe(
      switchMap(result => result ? this.storyService.delete(this.productId, story) : of(null))
    ).subscribe((rtn: Message) => {
      if (rtn && rtn.success) {
        this.getData();
      }
    });
  }

  onEdit(story: Story): void {
    const dialogRef: MatDialogRef<StoryEditComponent> = this.dialog.open(
      StoryEditComponent,
      {
        width: '500px',
        data: {
          isNew: false,
          productId: this.productId,
          epicId: story.EpicId,
          featureId: story.FeatureId,
          story
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result && !!result.length) {
        this.getData();
      }
    });
  }

  onView(story: Story): void {
    const dialogRef: MatDialogRef<StoryComponent> = this.dialog.open(
      StoryComponent,
      {
        width: '500px',
        data: {
          productId: this.productId,
          story
        }
      }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'edit') {
        this.onEdit(story);
      } else if (result === 'delete') {
        this.getData();
      }
    });
  }

  private getData(): void {
    this.showSpinner = true;
    this.storyService.getAll(this.productId).subscribe(
      (backlog: Story[]) => {
        const storyList = this.completeOnly ? backlog.filter(s => !!s.IsComplete) : backlog;
        this.hasStories = !!storyList.length;
        this.storiesData = new MatTableDataSource(storyList);
        this.storiesData.paginator = this.paginator;
        this.storiesData.sort = this.sort;
        this.table.dataSource = this.storiesData;
        this.stories = backlog;
        this.showSpinner = false;
      },
      () => {
        this.hasStories = false;
        this.showSpinner = false;
      }
    );
  }
}
