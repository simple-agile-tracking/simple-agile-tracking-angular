import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '@cores/auth-guard.service';

import { ProductDetailComponent } from './views/product-detail.component';

import { EpicListResolver } from './backlog/services/epic-resolver.service';

import { BacklogComponent } from './backlog/backlog.component';
import { EpicComponent } from './backlog/epic.component';
import { EpicListComponent } from './backlog/epic-list.component';

import { FeatureComponent } from './backlog/features/feature.component';

export const ProductRoutes: Routes = [
  {
    path: ':prodid', component: ProductDetailComponent, canActivate: [AuthGuard], children: [
    { path: 'backlog', component: BacklogComponent },
    { path: 'epics/:id', component: EpicComponent },
    { path: 'epics/:epicid/feature/:id', component: FeatureComponent },
    { path: 'epics', component: EpicListComponent, resolve: { epics: EpicListResolver } },
    { path: 'releases', loadChildren: () => import('app/products/releases/releases.module').then(m => m.ReleasesModule) },
    { path: 'sprints', loadChildren: () => import('app/products/sprints/sprints.module').then(m => m.SprintsModule) },
    { path: '', redirectTo: 'epics' }
  ] }
];

@NgModule({
  imports: [
    RouterModule.forChild(ProductRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProductRouting { }
