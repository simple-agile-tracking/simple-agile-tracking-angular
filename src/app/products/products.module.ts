import { NgModule } from '@angular/core';

import { SharedModule } from '@shares/shared.module';
import { SpinnerModule } from '@shares/spinner/spinner.module';

import { ProductListComponent } from './views/product-list.component';
import { ProductEditComponent } from './views/product-edit.component';

import { ProductsRouting } from './products-routing.module';

@NgModule({
  imports: [
    SharedModule,
    SpinnerModule,
    ProductsRouting
  ],
  declarations: [
    ProductListComponent,
    ProductEditComponent
  ],
  entryComponents: [
    ProductEditComponent
  ]
})
export class ProductsModule { }
