import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { DialogDeleteComponent } from '@shares/dialog.delete.component';

import { AuthService } from '@cores/auth.service';
import { Message } from '@cores/message.service';

import { Product, ProductService } from '../services/product.service';

import { ProductEditComponent } from './product-edit.component';

@Component({
  templateUrl: 'product-list.component.html'
})
export class ProductListComponent implements OnInit {
  @ViewChild(MatTable, { static: true }) table: MatTable<Product>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  hasProducts = false;
  displayedColumns = ['Name', 'Description', 'SprintLength', 'InBacklogPhase', 'Actions'];
  showSpinner = false;
  isAdmin: boolean;
  private productsData: MatTableDataSource<Product>;

  constructor(
    public dialog: MatDialog,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly productService: ProductService
  ) { }

  ngOnInit() {
    this.isAdmin = this.authService.u.admin;
    this.getData();
  }

  applyFilter(filterValue: string): void {
    this.productsData.filter = filterValue.trim().toLowerCase();
  }

  onAdd(): void {
    if (this.isAdmin) {
      const dialogRef: MatDialogRef<ProductEditComponent> = this.dialog.open(
        ProductEditComponent,
        {
          width: '500px',
          data: {
            isNew: true
          }
        }
      );

      dialogRef.afterClosed().subscribe(result => {
        if (result && !!result.length) {
          this.getData();
        }
      });
    }
  }

  onDelete(productId: string): void {
    if (this.isAdmin) {
      const dialogRef: MatDialogRef<DialogDeleteComponent> = this.dialog.open(
        DialogDeleteComponent,
        {
          width: '500px',
          data: {
            recordType: 'Product'
          }
        }
      );

      dialogRef.afterClosed().pipe(
        switchMap(result => result ? this.productService.delete(productId) : of(null))
      ).subscribe((rtn: Message) => {
        if (rtn && rtn.success) {
          this.getData();
        }
      });
    }
  }

  onEdit(product: Product): void {
    if (this.isAdmin) {
      const dialogRef: MatDialogRef<ProductEditComponent> = this.dialog.open(
        ProductEditComponent,
        {
          width: '500px',
          data: {
            isNew: false,
            product
          }
        }
      );

      dialogRef.afterClosed().subscribe(result => {
        if (result && !!result.length) {
          this.getData();
        }
      });
    }
  }

  onView(product: Product): void {
    if (product.InBacklogPhase) {
      this.router.navigate(['/products', 'p', product.Id, 'epics']);
    } else {
      this.router.navigate(['/products', 'p', product.Id, 'sprints']);
    }
  }

  private getData(): void {
    this.showSpinner = true;
    this.productService.getAll().subscribe(
      (products: Product[]) => {
        this.hasProducts = !!products.length;
        this.productsData = new MatTableDataSource(products);
        this.productsData.sort = this.sort;
        this.table.dataSource = this.productsData;
        this.showSpinner = false;
      },
      () => {
        this.hasProducts = false;
        this.showSpinner = true;
      }
    );
  }
}
