import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { IdService } from '@cores/id.service';
import { Message } from '@cores/message.service';

import { Product, ProductService } from '../services/product.service';

@Component({
  templateUrl: 'product-edit.component.html'
})
export class ProductEditComponent implements OnInit {
  productForm: FormGroup;
  isNew: boolean;
  sprintLengths = [
    { value: 1, display: '1 week' },
    { value: 2, display: '2 weeks' },
    { value: 3, display: '3 weeks' },
    { value: 4, display: '4 weeks' }
  ];
  title: string;
  private product: Product;

  constructor(
    public dialogRef: MatDialogRef<ProductEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { isNew: boolean, product: Product },
    private readonly formBuilder: FormBuilder,
    private readonly idService: IdService,
    private readonly productService: ProductService
  ) { }

  ngOnInit() {
    this.isNew = this.data.isNew;
    this.title = (this.isNew ? 'Add' : 'Edit') + ' Product';
    if (this.data.product) {
      this.product = this.data.product;
    }
    this.initForm(this.product);
  }

  get Name() { return this.productForm.get('Name'); }
  get Description() { return this.productForm.get('Description'); }

  onSubmit(): void {
    const newProduct = this.productForm.value;
    let saver: Observable<Message>;

    if (this.isNew) {
      newProduct.Id = this.idService.newID();
      saver = this.productService.insert(newProduct);
    } else {
      saver = this.productService.update(this.product, newProduct);
    }

    saver.subscribe((rtn: Message) => {
      if (rtn.success) {
        this.dialogRef.close('ok');
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  private initForm(product: Product): void {
    const productName = product ? product.Name : '';
    const productDesc = product ? product.Description : '';
    const sprtLength = product ? product.SprintLength.toString() : '';
    const backlogPhase = product ? product.InBacklogPhase : true;

    this.productForm = this.formBuilder.group({
      Name: [productName, Validators.required],
      Description: [productDesc, Validators.required],
      SprintLength: sprtLength,
      InBacklogPhase: backlogPhase
    });
  }
}
