import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DataMessage } from '@cores/message.service';

import { Product, ProductService } from '../services/product.service';

@Component({
  templateUrl: 'product-detail.component.html'
})
export class ProductDetailComponent implements OnInit {
  selectedProduct: Product;
  selectedId: string;
  phase = 'Backlog';
  hidden = true;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly productService: ProductService
  ) { }

  ngOnInit() {
    this.selectedId = this.route.snapshot.paramMap.get('prodid');
    this.productService.getById(this.selectedId).subscribe(
      (product: DataMessage<Product>) => {
        if (product.success) {
          this.selectedProduct = product.data;
          this.phase = this.selectedProduct.InBacklogPhase ? 'Backlog' : 'Development';
        } else {
          this.router.navigate(['/products']);
        }
      }
    );
  }

  onDetails() {
    this.hidden = !this.hidden;
  }
}
