import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@envs/environment';

import { IdService } from '@cores/id.service';

const server = environment.server;

@Injectable({
  providedIn: 'root'
})
export class LogService {
  constructor(private readonly http: HttpClient, private readonly idService: IdService) { }

  log(path: string): void {
    const logData = {
      Id: this.idService.newID(),
      ApplicationKey: 'sat',
      Path: path
    };

    this.http.post(
      server + '/api/core/log',
      JSON.stringify(logData)
    ).subscribe();
  }
}
