import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {
  download(dataStream: Blob, fileName: string): void {
    const binaryData = [];
    binaryData.push(dataStream);
    const downloadLink = document.createElement('a');
    const ref = window.URL.createObjectURL(new Blob(binaryData));
    downloadLink.href = ref;
    downloadLink.download = fileName + '.xlsx';
    document.body.appendChild(downloadLink);
    downloadLink.click();
    window.URL.revokeObjectURL(ref);
  }
}
