import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { User } from 'firebase';
import { Observable, of, from } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private readonly firebaseAuth: AngularFireAuth,
    private readonly authService: AuthService,
    private readonly router: Router
  ) { }

  canActivate(_next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.firebaseAuth.authState.pipe(
      switchMap((user: User) => {
        let rtn: Observable<boolean>;
        if (user) {
          rtn = from(user.getIdTokenResult().then(
            t => {
              if (!t.token) {
                this.router.navigate(['/not-authorized']);
              } else if (!!t.claims.deleted) {
                this.router.navigate(['/deleted']);
              } else if (!!t.claims.banned) {
                this.router.navigate(['/banHammer']);
              } else if (user.emailVerified) {
                return true;
              } else {
                this.router.navigate(['/verify']);
              }

              return false;
            }
          ));
        } else {
          this.authService.redirectURL = state.url;
          this.router.navigate(['/login']);
          rtn = of(false);
        }

        return rtn;
      }),
      catchError(() => {
        this.router.navigate(['/home']);
        return of(false);
      })
    );
  }
}
