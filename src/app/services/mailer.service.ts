import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@envs/environment';

import { FBAuthUser } from '@cores/auth.service';
import { Message, MessageService } from '@cores/message.service';

const server = environment.server;

export interface Mail {
  Subject: string;
  EmailTo: string;
  Message: string;
}

@Injectable({
  providedIn: 'root'
})
export class MailerService {
  constructor(private readonly http: HttpClient, private readonly msgService: MessageService) { }

  send(content: Mail): Observable<Message> {
    return this.msgService.fromHTTP(
      this.http.post(server + '/api/send', content),
      'Email sent successfully.',
      'Error Sending Email',
      true
    );
  }

  formatForm(formStruct: FBAuthUser): string {
    let output = '<p>';

    for (const [key, value] of Object.entries(formStruct)) {
      if (key === 'password') {
        output += key + ': ********' + '<br>';
      } else {
        output += key + ': ' + value + '<br>';
      }
    }

    output += '</p>';

    return output;
  }
}
