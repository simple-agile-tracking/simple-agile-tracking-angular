import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError, take, filter, switchMap, finalize } from 'rxjs/operators';

import { AuthService } from '@cores/auth.service';

@Injectable()
export class HttpAuthInterceptor implements HttpInterceptor {
  private authParams: { access_token: string; x_un: string; x_key: string; };
  private refreshAuthInProgress = false;
  private readonly refreshAuthSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private readonly firebaseAuth: AngularFireAuth, private readonly authService: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({ setHeaders: { 'content-type': 'application/json' } });

    req = this.addAuthParams(req);

    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse, _caught) => {
        if (error && error.status === 401) {
          // 401 is due to expired or invalid auth params, so refresh
          let rtn: Observable<HttpEvent<any>>;
          if (this.refreshAuthInProgress) {
            // if refreshAuthInProgress is true, wait until refreshAuthSubject has a non-null value to retry
            rtn = this.refreshAuthSubject.pipe(
              filter(result => result !== null),
              take(1),
              switchMap(() => next.handle(this.addAuthParams(req)))
            );
          } else {
            this.refreshAuthInProgress = true;
            // set refreshAuthSubject to null so that subsequent API calls will wait until the new auth params have been retrieved
            this.refreshAuthSubject.next(null);

            rtn = this.refreshAuthParams().pipe(
              switchMap((user: User) => {
                this.refreshAuthSubject.next(!!(user && user.emailVerified));
                return next.handle(this.addAuthParams(req));
              }),
              // when the call to refreshAuthParams completes, reset refreshAuthInProgress to false
              finalize(() => this.refreshAuthInProgress = false)
            );
          }

          return rtn;
        }

        return throwError(error);
      })
    );
  }

  private addAuthParams(request: HttpRequest<any>): HttpRequest<any> {
    // don't add auth params to public routes
    if (!request.url.match(/\/api\/(app|core)\//)) {
      return request;
    }
    this.authParams = this.authService.getParams();
    // don't add auth params if they don't exist yet
    if (this.authParams.access_token === '') {
      return request;
    }
    // add the auth params to the headers
    return request.clone({ setHeaders: this.authParams });
  }

  private refreshAuthParams(): Observable<User> {
    // subscribe to the Firebase AuthState to see if the auth params can be set
    return this.firebaseAuth.authState;
  }
}
