import { Injectable } from '@angular/core';
import * as cuid from 'cuid';

@Injectable({
  providedIn: 'root'
})
export class IdService {
  newID(): string {
    return cuid();
  }
}
