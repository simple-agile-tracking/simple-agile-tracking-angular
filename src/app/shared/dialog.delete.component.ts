import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

class DialogDelete {
  constructor(
    public recordType: string,
    public remType?: string
  ) { }
}

@Component({
  templateUrl: 'dialog.delete.component.html'
})
export class DialogDeleteComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DialogDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogDelete
  ) { }

  ngOnInit() {
    if (!this.data.remType) {
      this.data.remType = 'Delete';
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
