import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'yesNoBool' })
export class BooleanPipe implements PipeTransform {
  transform(rawText: boolean): string {
    return rawText ? 'Yes' : 'No';
  }
}
