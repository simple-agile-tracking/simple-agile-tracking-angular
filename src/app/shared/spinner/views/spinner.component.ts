import { Component, Input } from '@angular/core';

@Component({
  selector: 'sat-spinner',
  templateUrl: 'spinner.component.html'
})
export class SpinnerComponent {
  @Input() show = false;
  @Input() set size(data: 'large' | 'small') {
    if (data === 'small') {
      this.pixels = 120;
      this.showText = false;
      this.wrapperClass = 'spinner-bucket-small';
    }
  }

  pixels = 160;
  showText = true;
  wrapperClass = 'spinner-bucket';
}
