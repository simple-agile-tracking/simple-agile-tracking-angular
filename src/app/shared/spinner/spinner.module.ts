import { NgModule } from '@angular/core';

import { SharedModule } from '@shares/shared.module';

import { SpinnerComponent } from './views/spinner.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    SpinnerComponent
  ],
  exports: [
    SpinnerComponent
  ]
})
export class SpinnerModule { }
