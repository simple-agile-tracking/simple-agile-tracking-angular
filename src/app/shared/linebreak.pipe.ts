import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'lineBreakBRs' })
export class LineBreakPipe implements PipeTransform {
  transform(rawText: string): string {
    if (rawText) {
      return rawText.replace(/\n/g, '<br>');
    }

    return '';
  }
}
