import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export class Dialog {
  constructor(
    public title: string,
    public content: string
  ) { }
}

@Component({
  templateUrl: 'dialog.component.html'
})
export class DialogComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Dialog
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
